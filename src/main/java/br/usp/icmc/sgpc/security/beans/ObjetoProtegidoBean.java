/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.security.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "objeto_protegido")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "ObjetoProtegidoBean.findAll", query = "SELECT o FROM ObjetoProtegidoBean o ORDER By o.nome"),
    @NamedQuery(name = "ObjetoProtegidoBean.findById", query = "SELECT o FROM ObjetoProtegidoBean o WHERE o.id = :id"),
    @NamedQuery(name = "ObjetoProtegidoBean.findByNome", query = "SELECT o FROM ObjetoProtegidoBean o WHERE o.nome = :nome"),
    @NamedQuery(name = "ObjetoProtegidoBean.findByKeyword", query = "SELECT o FROM ObjetoProtegidoBean o WHERE o.nome LIKE :nome")})
public class ObjetoProtegidoBean implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="objeto_protegido_id_objeto_protegido_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_objeto_protegido", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 100, unique=true)
    private String nome;

    @Basic(optional = true)
    @Column(name = "descricao", nullable = false)
    private String descricao;

    @ManyToMany(mappedBy="objetosProtegidos", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private List<PapelBean> papeis;

    public ObjetoProtegidoBean() {
        this.papeis = new ArrayList<PapelBean>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<PapelBean> getPapeis() {
        return papeis;
    }

    public void setPapeis(List<PapelBean> papeis) {
        this.papeis = papeis;
    }

    @Override
    public boolean equals(Object obj) {
        return this.id.intValue() == ((ObjetoProtegidoBean)obj).getId().intValue();
    }
}
