/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.security;

/**
 *
 * @author herick
 */
public class ConfigConstantes {

    public static final String CONFIG_MAIL_HOST = "CONFIG_MAIL_HOST";
    public static final String CONFIG_MAIL_USERNAME = "CONFIG_MAIL_USERNAME";
    public static final String CONFIG_MAIL_PASSWORD = "CONFIG_MAIL_PASSWORD";
    public static final String CONFIG_MAIL_ADDRESS = "CONFIG_MAIL_ADDRESS";
    public static final String CONFIG_MAIL_EXIBITION_NAME = "CONFIG_MAIL_EXIBITION_NAME";

    public static final String CONFIG_NOTIFICACAO_PRESTACAO_RELATORIO = "CONFIG_NOTIFICACAO_PRESTACAO_RELATORIO";
    public static final String CONFIG_NOTIFICACAO_COTACAO = "CONFIG_NOTIFICACAO_COTACAO";


    public static final String CONFIG_AUDITORIA_ADICIONAR_AUXILIO = "CONFIG_AUDITORIA_ADICIONAR_AUXILIO";
    public static final String CONFIG_AUDITORIA_EDITAR_AUXILIO = "CONFIG_AUDITORIA_EDITAR_AUXILIO";
    public static final String CONFIG_AUDITORIA_REMANEJAR_AUXILIO = "CONFIG_AUDITORIA_REMANEJAR_AUXILIO";
    public static final String CONFIG_AUDITORIA_STATUS_AUXILIO = "CONFIG_AUDITORIA_STATUS_AUXILIO";

    public static final String CONFIG_AUDITORIA_TRANSPOSICAO_ALINEA = "CONFIG_AUDITORIA_TRANSPOSICAO_ALINEA";

    public static final String CONFIG_AUDITORIA_STATUS_PROJETO = "CONFIG_AUDITORIA_STATUS_PROJETO";
    public static final String CONFIG_AUDITORIA_EDITAR_PROJETO = "CONFIG_AUDITORIA_EDITAR_PROJETO";
    public static final String CONFIG_AUDITORIA_ADICIONAR_PROJETO = "CONFIG_AUDITORIA_ADICIONAR_PROJETO";
    public static final String CONFIG_AUDITORIA_DEL_PARTICIPANTE_PROJETO = "CONFIG_AUDITORIA_DEL_PARTICIPANTE_PROJETO";
    public static final String CONFIG_AUDITORIA_ADD_PARTICIPANTE_PROJETO = "CONFIG_AUDITORIA_ADD_PARTICIPANTE_PROJETO";

    public static final String CONFIG_AUDITORIA_LOGIN = "CONFIG_AUDITORIA_LOGIN";
    public static final String CONFIG_AUDITORIA_ERRO_LOGIN = "CONFIG_AUDITORIA_ERRO_LOGIN";

    public static final String CONFIG_AUDITORIA_ADICIONAR_PESSOA = "CONFIG_AUDITORIA_ADICIONAR_PESSOA";
    public static final String CONFIG_AUDITORIA_EDITAR_PESSOA = "CONFIG_AUDITORIA_EDITAR_PESSOA";
    public static final String CONFIG_AUDITORIA_SENHA_PESSOA = "CONFIG_AUDITORIA_SENHA_PESSOA";

    public static final String CONFIG_AUDITORIA_SENHA_EMAIL = "CONFIG_AUDITORIA_SENHA_EMAIL";
    public static final String CONFIG_AUDITORIA_SYNC_SENHA = "CONFIG_AUDITORIA_SYNC_SENHA";
    
    public static final String CONFIG_AUDITORIA_EMAIL_CONTAS_VENCER = "CONFIG_AUDITORIA_EMAIL_CONTAS_VENCER";
    public static final String CONFIG_AUDITORIA_EMAIL_CONTAS_ATAF = "CONFIG_AUDITORIA_EMAIL_CONTAS_ATAF";

    public static final String CONFIG_RECIBO_TEXTO = "CONFIG_RECIBO_TEXTO";
    
    public static final String CONFIG_DEFAULT_UNIDADE = "CONFIG_DEFAULT_UNIDADE";
    
    public static final String CONFIG_AUDITORIA_ADD_PARTICIPANTE_AUXILIO = "CONFIG_AUDITORIA_ADD_PARTICIPANTE_AUXILIO";
    
}
