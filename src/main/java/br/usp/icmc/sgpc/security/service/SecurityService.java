/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.security.service;

import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.security.beans.ObjetoProtegidoBean;
import br.usp.icmc.sgpc.security.beans.PapelBean;
import br.usp.icmc.sgpc.security.dao.ObjetoProtegidoJpaDao;
import br.usp.icmc.sgpc.security.dao.PapelJpaDao;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Artur
 */
public class SecurityService {

    private static SecurityService instance = new SecurityService();
    Logger logger = Logger.getLogger(this.getClass());

    private SecurityService() {
    }

    public static SecurityService getInstance() {
        return instance;
    }

    public List<PapelBean> listarPapeis() {
        List<PapelBean> listaPapeis = new ArrayList<PapelBean>();
        listaPapeis = new PapelJpaDao().findEntities(true, 0, 0, "nome");

        return listaPapeis;
    }

    public List<PapelBean> pesquisarPapel(String pesquisa) {
        List<PapelBean> listaPapeis = new ArrayList<PapelBean>();
        listaPapeis = new PapelJpaDao().pesquisar(pesquisa);

        return listaPapeis;
    }

    public PapelBean buscarPapel(Integer id) {
        return new PapelJpaDao().findEntity(id);
    }
    
    public PapelBean buscarPapel(String nome) {
        List<PapelBean> listaPapeis = new PapelJpaDao().pesquisar(nome);
        if(listaPapeis != null && !listaPapeis.isEmpty()){
            return listaPapeis.get(0);
        }else{
            return null;
        }
        
    }

    public List<ObjetoProtegidoBean> listarObjetosProtegidos() {
        List<ObjetoProtegidoBean> listaObjetosProtegidos = new ArrayList<ObjetoProtegidoBean>();
        listaObjetosProtegidos = new ObjetoProtegidoJpaDao().pesquisar();

        return listaObjetosProtegidos;
    }

    public List<ObjetoProtegidoBean> pesquisarObjetosProtegidos(String pesquisa) {
        List<ObjetoProtegidoBean> listaObjetosProtegidos = new ArrayList<ObjetoProtegidoBean>();
        listaObjetosProtegidos = new ObjetoProtegidoJpaDao().pesquisar(pesquisa);

        return listaObjetosProtegidos;
    }

    public boolean verificarPermissao(PessoaBean usuario, String nomeRecurso) {
        List<PapelBean> gruposUsuario = usuario.getPapeis();
        List<ObjetoProtegidoBean> objetosProtegidos = new ArrayList<ObjetoProtegidoBean>();
        PapelBean roleTemp;
        boolean temAcesso = false;
        for (int i = 0; i < gruposUsuario.size() && !temAcesso; i++) {
            roleTemp = gruposUsuario.get(i);
            objetosProtegidos = roleTemp.getObjetosProtegidos();
            for (int j = 0; j < objetosProtegidos.size() && !temAcesso; j++) {
                temAcesso = temAcesso || objetosProtegidos.get(j).getNome().equals(nomeRecurso);
            }
        }
        return temAcesso;
    }

    public void cadastraObjetoProtegido(ObjetoProtegidoBean objetoProtegido) {
        new ObjetoProtegidoJpaDao().create(objetoProtegido);
    }

    public void atualizarObjetoProtegido(ObjetoProtegidoBean objetoProtegido) {
        new ObjetoProtegidoJpaDao().update(objetoProtegido);
    }

    public void excluirObjetoProtegido(ObjetoProtegidoBean objetoProtegido) {
        new ObjetoProtegidoJpaDao().delete(objetoProtegido);
    }

    public void cadastraPapel(PapelBean papel) {
        new PapelJpaDao().create(papel);
    }

    public void atualizarPapel(PapelBean papel) {
        new PapelJpaDao().update(papel);
    }

    public void excluirPapel(PapelBean papel) {
        new PapelJpaDao().delete(papel);
    }

    public ObjetoProtegidoBean getObjetoProtegido(Integer id) {
        return new ObjetoProtegidoJpaDao().findEntity(id);
    }
}
