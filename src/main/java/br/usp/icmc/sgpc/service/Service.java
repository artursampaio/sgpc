/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.service;

import br.usp.icmc.sgpc.beans.*;
import br.usp.icmc.sgpc.dao.*;
import br.usp.icmc.sgpc.fmw.USPException;
import br.usp.icmc.sgpc.interfaces.CompromissoInterface;
import br.usp.icmc.sgpc.security.beans.PapelBean;
import br.usp.icmc.sgpc.security.dao.PapelJpaDao;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Artur
 */
public class Service implements Serializable{
    /*
     * Objeto para implementacao do singleton
     */

    private static Service instance = new Service();
    Logger logger = Logger.getLogger(this.getClass());

    /*
     * Metodo construtor privado, para evitar
     * multipla instanciacao
     */
    private Service() {
    }

    /*
     * Metodo que retorna a unica instancia da classe
     */
    public static Service getInstance() {
        return instance;
    }

    public String cadastraNovoUsuario(PessoaBean pessoa) throws Exception {
        String encryptedPassword = AutenticacaoService.getInstance().encryptPassword(pessoa.getPassword());
        pessoa.setPassword(encryptedPassword);
        new PessoaJpaDao().create(pessoa);
        return null;
    }

    public String cadastraNovoProjeto(ProjetoBean projeto) throws Exception {
        new ProjetoJpaDao().create(projeto);
        return null;
    }

    public String cadastraNovoAuxilio(AuxilioBean auxilio) throws Exception {
        new AuxilioJpaDao().create(auxilio);
        return null;
    }

    public ProjetoBean cadastraProjeto(ProjetoBean projetoBean) {
//        PessoaBean responsavel = new PessoaJpaDao().findByUsername("artur");
//        projetoBean.setFkResponsavel(responsavel);
//        projetoBean.setStatus("NOVO");
        return new ProjetoJpaDao().create(projetoBean);
    }

    public AuxilioBean cadastraAuxilio(AuxilioBean auxilioBean) {
//        PessoaBean responsavel = new PessoaJpaDao().findByUsername("artur");
//        auxilioBean.setStatus("PLANEJAMENTO");
        return new AuxilioJpaDao().create(auxilioBean);
    }

    public String cadastraContaCorrente(ContaCorrenteBean contaCorrenteBean) {
        new ContaCorrenteJpaDao().create(contaCorrenteBean);
        return null;
    }

    public String cadastraCentroDespesa(CentroDespesaBean centroDespesaBean) {
        new CentroDespesaJpaDao().create(centroDespesaBean);
        return null;
    }

    public String cadastraInstituicao(InstituicaoBean instituicaoBean) {
        new InstituicaoJpaDao().create(instituicaoBean);
        return null;
    }

    public String cadastraInstituicaoBancaria(InstituicaoBancariaBean instituicaoBancariaBean) {
        new InstituicaoBancariaJpaDao().create(instituicaoBancariaBean);
        return null;
    }

    public PessoaBean cadastraUsuario(PessoaBean pessoaBean) {
        return new PessoaJpaDao().create(pessoaBean);
    }

    public String cadastraFinanciador(FinanciadorBean financiadorBean) {
        new FinanciadorJpaDao().create(financiadorBean);
        return null;
    }

    public String cadastraProdutos(ProdutoBean produtosBean) {
        new ProdutoJpaDao().create(produtosBean);
        return null;
    }

    public List<InstituicaoBancariaBean> listarInstituicoesBancarias() {
        return new InstituicaoBancariaJpaDao().findEntities();
    }

    /**
     * Retorna a lista de pessoas por ordem alfabética.
     *
     * @return
     */
    public List<PessoaBean> listarPessoas() {
        return new PessoaJpaDao().pesquisar();
    }

    public List<PessoaBean> pesquisarPessoas(String termoPesquisa) {
        return new PessoaJpaDao().pesquisar(termoPesquisa);
    }

    public void atualizarInstituicaoBancaria(InstituicaoBancariaBean instituicaoBancaria) {
        logger.debug("atualizarInstituicaoBancaria");
        new InstituicaoBancariaJpaDao().update(instituicaoBancaria);
    }

    public PessoaBean atualizarPessoa(PessoaBean pessoaBean) {
        logger.debug("atualizarPessoa");
        return new PessoaJpaDao().update(pessoaBean);
    }

    public ProjetoBean atualizarProjeto(ProjetoBean projetoBean) {
        logger.debug("atualizarProjeto");
        return new ProjetoJpaDao().update(projetoBean);
    }

    public ContaCorrenteBean atualizarContaCorrente(ContaCorrenteBean contaCorrente) {
        logger.debug("atualizarContaCorrente");
        return new ContaCorrenteJpaDao().update(contaCorrente);
    }

    public void atualizarCentroDespesa(CentroDespesaBean centroDespesa) {
        logger.debug("atualizarCentroDespesa");
        new CentroDespesaJpaDao().update(centroDespesa);
    }

    public AuxilioBean atualizarAuxilio(AuxilioBean auxilio) {
        logger.debug("atualizarAuxilio");
        return new AuxilioJpaDao().update(auxilio);
    }

    public void atualizarPrestacaoContas(PrestacaoContasBean contas) {
        logger.debug("atualizarPrestacaoContas");
        new PrestacaoContasJpaDao().update(contas);
    }

    public void atualizarRelatorioCientifico(RelatorioCientificoBean relatorio) {
        logger.debug("atualizarRelatorioCientifico");
        new RelatorioCientificoJpaDao().update(relatorio);
    }

    public void atualizarAlinea(AlineaBean alinea) {
        logger.debug("atualizarAlinea");
        new AlineaJpaDao().update(alinea);
    }

    public void atualizarInstituicao(InstituicaoBean instituicaoBean) {
        logger.debug("atualizarInstituicao");
        new InstituicaoJpaDao().update(instituicaoBean);
    }

    public FinanciadorBean atualizarFinanciador(FinanciadorBean financiadorBean) {
        logger.debug("atualizarFinanciador");
        return new FinanciadorJpaDao().update(financiadorBean);
    }

    public void atualizarFornecedor(FornecedorBean fornecedorBean) {
        logger.debug("atualizarFornecedor");
        new FornecedorJpaDao().update(fornecedorBean);
    }

    public void excluirContaCorrente(ContaCorrenteBean contaCorrente) {
        new ContaCorrenteJpaDao().delete(contaCorrente);
    }

    public void excluirProdutos(ProdutoBean produtos) {
        new ProdutoJpaDao().delete(produtos);
    }

    public void excluirCentroDespesa(CentroDespesaBean centroDespesa) {
        new CentroDespesaJpaDao().delete(centroDespesa);
    }

    public void excluirInstituicao(InstituicaoBean instituicaoBean) {
        new InstituicaoJpaDao().delete(instituicaoBean);
    }

    public void excluirInstituicaoBancaria(InstituicaoBancariaBean instituicaoBancaria) {
        new InstituicaoBancariaJpaDao().delete(instituicaoBancaria);
    }

    public void excluirPessoa(PessoaBean pessoaBean) {
        new PessoaJpaDao().delete(pessoaBean);
    }

    public void excluirProjeto(ProjetoBean projetoBean) {
        new ProjetoJpaDao().delete(projetoBean);
    }

    public void excluirAuxilio(AuxilioBean auxilio) {
        logger.debug("excluirAuxilio");
        new AuxilioJpaDao().delete(auxilio);
    }

    public void excluirAlinea(AlineaBean alinea) {
        logger.debug("excluirAlinea");
        new AlineaJpaDao().delete(alinea);
    }

    public void excluirRelatorioCientifico(RelatorioCientificoBean relatorio) {
        logger.debug("excluirRelatorioCientifico");
        new RelatorioCientificoJpaDao().delete(relatorio);
    }

    public void excluirPrestacaoContas(PrestacaoContasBean contas) {
        logger.debug("excluirPrestacaoContas");
        new PrestacaoContasJpaDao().delete(contas);
    }

    public void excluirModalidade(ModalidadeBean modalidadeBean) {
        logger.debug("excluirModalidade");
        new ModalidadeJpaDao().delete(modalidadeBean);
    }

    public List<InstituicaoBancariaBean> pesquisarInstituicoesBancarias(String termoPesquisa) {
        return new InstituicaoBancariaJpaDao().pesquisar(termoPesquisa);
    }

    public PessoaBean pesquisarPessoa(PessoaBean pessoa) {
        return new PessoaJpaDao().findEntity(pessoa.getId());
    }

    public List<PessoaBean> pesquisarPessoas(String termoPesquisa, InstituicaoBean instituicao, UnidadeBean unidade, DepartamentoBean departamento, PapelBean papel) {
        return new PessoaJpaDao().pesquisarCriteria(termoPesquisa, instituicao, unidade, departamento, papel);
    }

    public List<PessoaBean> pesquisarPessoasFinanceiro() {
        List<PapelBean> listaPapeis = new PapelJpaDao().pesquisar("FINANCEIRO");
        if (listaPapeis != null && listaPapeis.get(0) != null) {
            return listaPapeis.get(0).getPessoas();
        } else {
            return new ArrayList<PessoaBean>();
        }
        //return new PessoaJpaDao().pesquisarGrupo(papel);
    }
    
    public List<PessoaBean> pesquisarPessoasReponsaveis() {
        List<PessoaBean> pessoasRetorno = new ArrayList<PessoaBean>();
        HashSet pessoas = new HashSet();
        List<PapelBean> papeisResponsaveis = new PapelJpaDao().listarPapeisResponsaveis(true);
        for(PapelBean papel : papeisResponsaveis){
            pessoas.addAll(papel.getPessoas());
        }
        pessoasRetorno.addAll(pessoas);
        Collections.sort(pessoasRetorno,PessoaBean.POR_NOME);
        return pessoasRetorno;
    }

    public List<PessoaBean> pesquisarPessoasDocente() {
        List<PapelBean> listaPapeis = new PapelJpaDao().pesquisar("DOCENTE");
        if (listaPapeis != null && listaPapeis.get(0) != null) {
            return listaPapeis.get(0).getPessoas();
        } else {
            return new ArrayList<PessoaBean>();
        }
        //return new PessoaJpaDao().pesquisarGrupo(papel);
    }

    public List<PessoaBean> pesquisarPessoas() {
        return new PessoaJpaDao().pesquisar();
    }

    public List<FinanciadorBean> listarFinanciadores() {
        return new FinanciadorJpaDao().findEntities(true, 0, 0, "sigla");
    }

    public List<FinanciadorBean> pesquisarFinanciadores(String termoPesquisa) {
        return new FinanciadorJpaDao().pesquisar(termoPesquisa);
    }

    public List<FornecedorBean> pesquisarFornecedor(String termoPesquisa) {
        return new FornecedorJpaDao().pesquisar(termoPesquisa);
    }

    public List<FornecedorBean> listarFornecedor() {
        return new FornecedorJpaDao().listar();
    }

    public FinanciadorBean pesquisarFinanciadorCnpj(String cnpj) {
        return new FinanciadorJpaDao().pesquisarCnpj(cnpj);
    }

    public List<BemPatrimoniadoBean> listarBensPatrimoniados() {
        return new BemPatrimoniadoJpaDao().findEntities();
    }

    public List<BemPatrimoniadoBean> pesquisarBensPatrimoniados(String termoPesquisa) {
        return new BemPatrimoniadoJpaDao().pesquisar(termoPesquisa);
    }

    public List<BemPatrimoniadoBean> pesquisarBensPatrimoniados(String termoPesquisa, Date dataInicio, Date dataFim, FinanciadorBean financiador) {
        return new BemPatrimoniadoJpaDao().pesquisarCriteria(termoPesquisa, dataInicio, dataFim, financiador);
    }

    /**
     * Retorna a lista de projetos em ordem alfabética
     *
     * @return
     */
    public List<ProjetoBean> listarProjetos() {
        return new ProjetoJpaDao().pesquisar();
    }

    public List<ProjetoBean> pesquisarProjetos(PessoaBean responsavel) {
        return new ProjetoJpaDao().pesquisarOwner(responsavel);
    }

    public List<ContaCorrenteBean> listarContasCorrentes() {
        return new ContaCorrenteJpaDao().findEntities();
    }

    public List<ContaCorrenteBean> pesquisarContasCorrentes(String termoPesquisa) {
        return new ContaCorrenteJpaDao().pesquisar(termoPesquisa);
    }

    public List<AuxilioBean> listarAuxilios() {
        return new AuxilioJpaDao().pesquisar();
    }

    public List<PrestacaoContasBean> listarPrestacoesContas() {
        return new PrestacaoContasJpaDao().findEntities(true, 0, 0, "dataLimite");
    }

    public List<RelatorioCientificoBean> listarRelatoriosCientificos() {
        return new RelatorioCientificoJpaDao().findEntities(true, 0, 0, "dataLimite");
    }

    public AuxilioBean buscarAuxilios(Integer id) {
        return new AuxilioJpaDao().findEntity(id);
    }

    public AlineaBean buscarAlinea(Integer id) {
        return new AlineaJpaDao().findEntity(id);
    }

    public DepartamentoBean buscarDepartamento(Integer id) {
        return new DepartamentoJpaDao().findEntity(id);
    }

    public PessoaBean buscarPessoa(Integer id) {
        return new PessoaJpaDao().findEntity(id);
    }

    public ProjetoBean buscarProjeto(Integer id) {
        return new ProjetoJpaDao().findEntity(id);
    }

    public InstituicaoBean buscarInstituicao(Integer id) {
        return new InstituicaoJpaDao().findEntity(id);
    }

    public CentroDespesaBean buscarCentroDespesa(Integer id) {
        return new CentroDespesaJpaDao().findEntity(id);
    }

    public FinanciadorBean buscarFinanciador(Integer id) {
        return new FinanciadorJpaDao().findEntity(id);
    }

    public ModalidadeBean buscarModalidade(Integer id) {
        return new ModalidadeJpaDao().findEntity(id);
    }

    public TipoAuxilioBean buscarTiposAuxilio(Integer id) {
        return new TipoAuxilioJpaDao().findEntity(id);
    }

    public List<InstituicaoBean> listarInstituicoes() {
        return new InstituicaoJpaDao().findEntities();
    }

    public List<InstituicaoBean> pesquisarInstituicoes(String termoPesquisa) {
        return new InstituicaoJpaDao().pesquisar(termoPesquisa);
    }

    public List<DepartamentoBean> listarDepartamentos() {
        return new DepartamentoJpaDao().findEntities();
    }

    public String cadastraDepartamento(DepartamentoBean departamento) {
        new DepartamentoJpaDao().create(departamento);
        return null;
    }

    public String cadastraFornecedor(FornecedorBean fornecedor) {
        new FornecedorJpaDao().create(fornecedor);
        return null;
    }

    public void atualizarDepartamento(DepartamentoBean departamento) {
        logger.debug("atualizarDepartamento");
        new DepartamentoJpaDao().update(departamento);
    }

    public void excluirDepartamento(DepartamentoBean departamento) {
        new DepartamentoJpaDao().delete(departamento);
    }

    public void excluirFinanciador(FinanciadorBean financiadorBean) {
        new FinanciadorJpaDao().delete(financiadorBean);
    }

    public void excluirTipoAuxilio(TipoAuxilioBean tipoAuxilio) {
        new TipoAuxilioJpaDao().delete(tipoAuxilio);
    }

    public List<DepartamentoBean> pesquisarDepartamentos(String termoPesquisa) {
        return new DepartamentoJpaDao().pesquisar(termoPesquisa);
    }

    /**
     * Retorna a lista de departamentos ordenados alfabeticamente pela Sigla
     *
     * @return
     */
    public List<DepartamentoBean> pesquisarDepartamentos() {
        return new DepartamentoJpaDao().pesquisar();
    }

    public List<CentroDespesaBean> listarCentrosDespesa() {
        return new CentroDespesaJpaDao().listarOrdenadaPorNome();
    }

    public List<CentroDespesaBean> pesquisarCentrosDespesa(String termoPesquisa) {
        return new CentroDespesaJpaDao().pesquisar(termoPesquisa);
    }

    public ProjetoBean pesquisaProjeto(Integer idProjeto) {
        return new ProjetoJpaDao().findEntity(idProjeto);
    }

    public PessoaBean pesquisarPessoaEmail(String email) throws USPException {
        return new PessoaJpaDao().findByEmail(email);
    }

    public PessoaBean pesquisarPessoaCPF(String cpf) throws USPException {
        return new PessoaJpaDao().findByCpf(cpf);
    }

    public List<ContaCorrenteBean> pesquisarContasCorrentes(PessoaBean responsavel) {
        return new ContaCorrenteJpaDao().pesquisarOwner(responsavel);
    }

    public List<ContaCorrenteBean> pesquisarContasCorrentes(PessoaBean responsavel, String termoPesquisa) {
        return new ContaCorrenteJpaDao().pesquisarOwner(responsavel, termoPesquisa);
    }

    public ContaCorrenteBean buscarContaCorrente(Integer idContaCorrente) {
        return new ContaCorrenteJpaDao().findEntity(idContaCorrente);
    }

    public InstituicaoBancariaBean buscarInstituicaoBancaria(Integer idInstituicaoBancaria) {
        return new InstituicaoBancariaJpaDao().findEntity(idInstituicaoBancaria);
    }

    public PessoaBean pesquisarPessoaUsername(String username) throws USPException {
        return new PessoaJpaDao().findByUsername(username);
    }

    public List<AuxilioBean> pesquisarAuxilios(PessoaBean user
            , String termoPesquisa, FinanciadorBean financiador
            , String status, Date dataInicial, Date dataFinal
            , int first, int pageSize, String sortField, String sortOrder
            , String numeroProcessoEntidade, Date vigenciaDataInicial
            , Date vigenciaDataFinal, DepartamentoBean departamento
            , String pesquisaModalidade, UnidadeBean unidade
            , PessoaBean responsavelAuxilio, PessoaBean responsavelProjeto
            , String tipoBolsaAuxilio
            , Date dataPesquisaInicioIniciadoEm
            , Date dataPesquisaFimIniciadoEm) {
        return new AuxilioJpaDao().pesquisarOwnerCriteria(user, termoPesquisa, financiador, status, dataInicial, dataFinal,first, pageSize, sortField, sortOrder, numeroProcessoEntidade, vigenciaDataInicial, vigenciaDataFinal, departamento, pesquisaModalidade, unidade, responsavelAuxilio, responsavelProjeto, tipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);
    }

    public int contarAuxilios(PessoaBean pessoa, String numeroProcessoEntidade
            , FinanciadorBean financiador, String status, Date dataInicio
            , Date dataTermino, Date vigenciaDataInicial
            , Date vigenciaDataFinal, DepartamentoBean departamento
            , String pesquisaModalidade, UnidadeBean unidade
            , PessoaBean responsavelAuxilio, PessoaBean responsavelProjeto
            , String tipoBolsaAuxilio
            , Date dataPesquisaInicioIniciadoEm
            , Date dataPesquisaFimIniciadoEm) {
        List<AuxilioBean> listaAuxilios = pesquisarAuxilios(pessoa, numeroProcessoEntidade, financiador, status, dataInicio, dataTermino, 0, 0, null, null, null, vigenciaDataInicial, vigenciaDataFinal, departamento, pesquisaModalidade, unidade, responsavelAuxilio, responsavelProjeto, tipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);
        return listaAuxilios.size();
        //return new AuxilioJpaDao().contarRegistros(pessoa, numeroProcessoEntidade, financiador, status, dataInicio, dataTermino);
    }

    public BigDecimal valorTotalAuxilios(PessoaBean pessoa
            , String numeroProcessoEntidade, FinanciadorBean financiador
            , String status, Date dataInicio, Date dataTermino
            , Date vigenciaDataInicial, Date vigenciaDataFinal
            , DepartamentoBean departamento, String pesquisaModalidade
            , UnidadeBean unidade
            , PessoaBean responsavelAuxilio, PessoaBean responsavelProjeto, int moeda
            , String tipoBolsaAuxilio
            , Date dataPesquisaInicioIniciadoEm
            , Date dataPesquisaFimIniciadoEm) {
        BigDecimal valor = new BigDecimal(0);
        List<AuxilioBean> listaAuxilios = pesquisarAuxilios(pessoa, numeroProcessoEntidade, financiador, status, dataInicio, dataTermino, 0, 0, null, null, null, vigenciaDataInicial, vigenciaDataFinal, departamento, pesquisaModalidade, unidade, responsavelAuxilio, responsavelProjeto, tipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);
        for (AuxilioBean aux : listaAuxilios) {
            valor = valor.add(ContabilidadeService.getInstance().verbaAprovadaAuxilio(aux,moeda));
        }
        return valor;
    }

    public List<PrestacaoContasBean> pesquisarPrestacoesContas(PessoaBean user, String termoPesquisa, Date dataInicio, Date dataFim, FinanciadorBean financiador, String status) {
        return new PrestacaoContasJpaDao().pesquisarCriteria(user, termoPesquisa, dataInicio, dataFim, financiador, status);
    }

    public List<RelatorioCientificoBean> pesquisarRelatoriosCientificos(PessoaBean user, String termoPesquisa, Date dataInicio, Date dataFim, FinanciadorBean financiador, String status) {
        return new RelatorioCientificoJpaDao().pesquisarCriteria(user, termoPesquisa, dataInicio, dataFim, financiador, status);
    }

    public DepartamentoBean buscarDepartamentoPorSigla(String sigla) {
        DepartamentoBean dept = null;
        List<DepartamentoBean> depts = new DepartamentoJpaDao().pesquisarPorSigla(sigla);
        if (depts != null && depts.size() > 0) {
            dept = depts.get(0);
        }
        return dept;
    }

    public List<ProjetoBean> pesquisarProjetos(PessoaBean user, String termoPesquisa, DepartamentoBean departamento, String status, Date dataInicial, Date dataFinal, PessoaBean responsavel) {
        return new ProjetoJpaDao().pesquisarOwnerCriteria(user, termoPesquisa, departamento, status, dataInicial, dataFinal, responsavel);
    }

    public String cadastraBemPatrimoniado(BemPatrimoniadoBean bemPatrimoniadoBean) {
        new BemPatrimoniadoJpaDao().create(bemPatrimoniadoBean);
        return null;
    }

    public String cadastraTipoAuxilio(TipoAuxilioBean tipoAuxilio) {
        new TipoAuxilioJpaDao().create(tipoAuxilio);
        return null;
    }

    public void atualizarBemPatrimoniado(BemPatrimoniadoBean bemPatrimoniadoBean) {
        logger.debug("atualizarInstituicao");
        new BemPatrimoniadoJpaDao().update(bemPatrimoniadoBean);
    }

    public TipoAuxilioBean atualizarTipoAuxilio(TipoAuxilioBean tipoAuxilio) {
        logger.debug("atualizarTipoAuxilio");
        return new TipoAuxilioJpaDao().update(tipoAuxilio);
    }

    public void atualizarProdutos(ProdutoBean produtosBean) {
        logger.debug("atualizarProdutos");
        new ProdutoJpaDao().update(produtosBean);
    }

    public void excluirBemPatrimoniado(BemPatrimoniadoBean bemPatrimoniadoBean) {
        new BemPatrimoniadoJpaDao().delete(bemPatrimoniadoBean);
    }

    public void excluirFornecedor(FornecedorBean fornecedorBean) {
        new FornecedorJpaDao().delete(fornecedorBean);
    }

    public List<TipoAuxilioBean> listarTiposAuxilio() {
        return new TipoAuxilioJpaDao().findEntities();
    }

    public List<TipoAuxilioBean> pesquisarTipoAuxilio(String termoPesquisa) {
        return new TipoAuxilioJpaDao().pesquisar(termoPesquisa);
    }

    public List<ProdutoBean> pesquisarProdutos(String termoPesquisa) {
        return new ProdutoJpaDao().pesquisar(termoPesquisa);
    }

    public PrestacaoContasBean buscarPrestacaoContas(Integer id) {
        return new PrestacaoContasJpaDao().findEntity(id);
    }

    public List<DespesaBean> listarDespesa() {
        return new DespesaJpaDao().findEntities();
    }

    public List<DespesaBean> pesquisarDespesa(String termoPesquisa) {
        return new DespesaJpaDao().pesquisar(termoPesquisa);
    }

    public String cadastraDespesa(DespesaBean despesa) {
        new DespesaJpaDao().create(despesa);
        return null;
    }

    public List<AtividadeBean> listarAtividade() {
        return new AtividadeJpaDao().findEntities();
    }

    public List<AtividadeBean> listarAtividade(EventoBean evento) {
        return new AtividadeJpaDao().pesquisar(evento);
    }

    public List<ProdutoBean> listarProdutos() {
        return new ProdutoJpaDao().findEntities();
    }

    public List<AtividadeBean> pesquisarAtividade(String termoPesquisa) {
        return new AtividadeJpaDao().pesquisar(termoPesquisa);
    }

    public AtividadeBean pesquisarAtividade(Integer id) {
        return new AtividadeJpaDao().findEntity(id);
    }

    public String cadastraAtividade(AtividadeBean atividadeBean) {
        new AtividadeJpaDao().create(atividadeBean);
        return null;
    }

    public void atualizarAtividade(AtividadeBean atividadeBean) {
        new AtividadeJpaDao().update(atividadeBean);
    }

    public void excluirAtividade(AtividadeBean atividade) {
        new AtividadeJpaDao().delete(atividade);
    }

    public String cadastraModalidade(ModalidadeBean modalidadeBean) {
        new ModalidadeJpaDao().create(modalidadeBean);
        return null;
    }

    public void atualizarModalidade(ModalidadeBean modalidadeBean) {
        logger.debug("atualizarModalildade");
        new ModalidadeJpaDao().update(modalidadeBean);
    }

    public List<DespesaBean> listarDespesa(AuxilioBean auxilio) {
        return new DespesaJpaDao().pesquisar(auxilio);
    }

    public void excluirDespesa(DespesaBean despesa) {
        new DespesaJpaDao().delete(despesa);
    }

    public String cadastraDocumentoComprovacaoDespesa(DocumentoComprovacaoDespesaBean documentoDespesa) {
        new DocumentoComprovacaoDespesaJpaDao().create(documentoDespesa);
        return null;
    }

    public void atualizarDespesa(DespesaBean despesa) {
        new DespesaJpaDao().update(despesa);
    }

    public void atualizarDocumentoComprovacaoDespesa(DocumentoComprovacaoDespesaBean documentoDespesa) {
        new DocumentoComprovacaoDespesaJpaDao().update(documentoDespesa);
    }

    public List<AuditoriaBean> pesquisarAuditoria() {
        return new AuditoriaJpaDao().pesquisar();
    }

    public String cadastraDocumentoComprovacaoPagamento(DocumentoComprovacaoPagamentoBean documentoPagamento) {
        new DocumentoComprovacaoPagamentoJpaDao().create(documentoPagamento);
        return null;
    }

    public void atualizarDocumentoComprovacaoPagamento(DocumentoComprovacaoPagamentoBean documentoPagamento) {
        new DocumentoComprovacaoPagamentoJpaDao().update(documentoPagamento);
    }

    public List<CompromissoInterface> getCompromissos(PessoaBean pessoa, Date dataLimite) {
        List<CompromissoInterface> compromissos = new ArrayList<CompromissoInterface>();
        List<PrestacaoContasBean> prestacoesContas = new PrestacaoContasJpaDao().getPrestacoesContas(pessoa, dataLimite);
        List<RelatorioCientificoBean> relatoriosCientificos = new RelatorioCientificoJpaDao().getRelatoriosCientificos(pessoa, dataLimite);
        compromissos.addAll(prestacoesContas);
        compromissos.addAll(relatoriosCientificos);
        return compromissos;
    }

    public List<CompromissoInterface> getCompromissos(int intervaloDias) {
        List<CompromissoInterface> compromissos = new ArrayList<CompromissoInterface>();
        List<PrestacaoContasBean> prestacoesContas = new PrestacaoContasJpaDao().getPrestacoesContas(intervaloDias);
        List<RelatorioCientificoBean> relatoriosCientificos = new RelatorioCientificoJpaDao().getRelatoriosCientificos(intervaloDias);
        compromissos.addAll(prestacoesContas);
        compromissos.addAll(relatoriosCientificos);
        return compromissos;
    }

    public List<PrestacaoContasBean> getPrestacoesContas(PessoaBean pessoa, Date dataLimite) {
        return new PrestacaoContasJpaDao().getPrestacoesContas(pessoa, dataLimite);
    }

    public List<PrestacaoContasBean> getPrestacoesContas(Date dataInicial, Date dataFinal) {
        return new PrestacaoContasJpaDao().getPrestacoesContas(dataInicial, dataFinal);
    }

    public List<PrestacaoContasBean> getPrestacoesContas(int intervaloDias) {
        return new PrestacaoContasJpaDao().getPrestacoesContas(intervaloDias);
    }

    public List<RelatorioCientificoBean> getRelatoriosCientificos(PessoaBean pessoa, Date dataLimite) {
        return new RelatorioCientificoJpaDao().getRelatoriosCientificos(pessoa, dataLimite);
    }

    public void atualizarEvento(EventoBean evento) throws Exception {
        new EventoJpaDao().update(evento);
    }

    public void excluirPessoaProjeto(PessoaProjetoBean pessoa) {
        new PessoaProjetoJpaDao().delete(pessoa);
    }

    public List<PrestacaoContasBean> getPrestacoesContas(PessoaBean pessoa, Date dtIni, Date dtFim) {
        return new PrestacaoContasJpaDao().getPrestacoesContas(pessoa, dtIni, dtFim);
    }

    public List<RelatorioCientificoBean> getRelatoriosCientificos(PessoaBean pessoa, Date dtIni, Date dtFim) {
        return new RelatorioCientificoJpaDao().getRelatoriosCientificos(pessoa, dtIni, dtFim);
    }

    public void excluirCategoriaAlinea(CategoriaAlineaBean alinea) {
        new CategoriaAlineaJpaDao().delete(alinea);
    }

    public List<CategoriaAlineaBean> listarCategoriaAlinea() {
        return new CategoriaAlineaJpaDao().findAll();
    }

    public List<CategoriaAlineaBean> pesquisarCategoriaAlinea(String termoPesquisa) {
        return new CategoriaAlineaJpaDao().pesquisar(termoPesquisa);
    }

    public void cadastraCategoriaAlinea(CategoriaAlineaBean alinea) {
        new CategoriaAlineaJpaDao().create(alinea);
    }

    public void atualizarCategoriaAlinea(CategoriaAlineaBean alinea) {
        new CategoriaAlineaJpaDao().update(alinea);
    }

    public List<AlineaBean> listarAlineas() {
        return new AlineaJpaDao().findEntities();
    }

    public CategoriaAlineaBean buscarCategoriaAlinea(int id) {
        return new CategoriaAlineaJpaDao().findEntity(id);
    }

    public List<AlineaBean> listarAlineaCapital(AuxilioBean aux) {
        return new AlineaJpaDao().listarCapital(aux, true);
    }

    public List<AlineaBean> listarAlineaCorrente(AuxilioBean aux) {
        return new AlineaJpaDao().listarCusteio(aux, true);
    }

    public List<AlineaBean> listarAlineaReservaTecnica(AuxilioBean aux) {
        return new AlineaJpaDao().listarReservaTecnica(aux, true);
    }
    
    public List<AlineaBean> listarAlineaBeneficioComplementar(AuxilioBean aux) {
        return new AlineaJpaDao().listarBeneficioComplementar(aux, true);
    }

    public List<DespesaBean> listarDespesa(PessoaBean usuarioLogado) {
        return new DespesaJpaDao().pesquisarOwner(usuarioLogado);
    }

    public ProgramaEspecialFinanciamentoBean buscarProgramaEspecialFinanciamento(int id) {
        return new ProgramaEspecialFinanciamentoJpaDao().findEntity(id);
    }

    public List<ProgramaEspecialFinanciamentoBean> listarProgramaEspecialFinanciamento() {
        return new ProgramaEspecialFinanciamentoJpaDao().findEntities();
    }

    public List<ProgramaEspecialFinanciamentoBean> pesquisarProgramaEspecialFinanciamento(String termoPesquisa) {
        return new ProgramaEspecialFinanciamentoJpaDao().pesquisar(termoPesquisa);
    }

    public void cadastraProgramaEspecialFinanciamento(ProgramaEspecialFinanciamentoBean programaEspecial) {
        new ProgramaEspecialFinanciamentoJpaDao().create(programaEspecial);
    }

    public void atualizarProgramaEspecialFinanciamento(ProgramaEspecialFinanciamentoBean programaEspecial) {
        logger.debug("atualizarProgramaEspecialFinanciamento");
        new ProgramaEspecialFinanciamentoJpaDao().update(programaEspecial);
    }

    public void excluirProgramaEspecialFinanciamento(ProgramaEspecialFinanciamentoBean programaEspecial) {
        new ProgramaEspecialFinanciamentoJpaDao().delete(programaEspecial);
    }

    public List<DespesaBean> pesquisarDespesa(PessoaBean user, String termoPesquisa, Date dataRealizada, FinanciadorBean financiador, String status, CentroDespesaBean centroDespesa, AuxilioBean auxilio, AlineaBean alinea, String tipoDespesa, Date dataInicialDocFiscal, Date dataFinalDocFiscal, BigDecimal valorPesquisa, Date dataInicialCheque, Date dataFinalCheque, String numeroCheque, String numeroDocFiscal) {
        return new DespesaJpaDao().pesquisarOwnerCriteria(user, termoPesquisa, dataRealizada, financiador, status, centroDespesa, auxilio, alinea, tipoDespesa, dataInicialDocFiscal, dataFinalDocFiscal, valorPesquisa, dataInicialCheque, dataFinalCheque, numeroCheque, numeroDocFiscal);
    }

    public void excluirUnidade(UnidadeBean unidade) {
        new UnidadeJpaDao().delete(unidade);
    }

    public void cadastraUnidade(UnidadeBean unidade) {
        new UnidadeJpaDao().create(unidade);
    }

    public void atualizarUnidade(UnidadeBean unidade) {
        new UnidadeJpaDao().update(unidade);
    }

    public List<UnidadeBean> listarUnidades(InstituicaoBean instituicao) {
        return new UnidadeJpaDao().pesquisar(instituicao);
    }

    public List<UnidadeBean> pesquisarUnidades(InstituicaoBean instituicao, String pesquisa) {
        return new UnidadeJpaDao().pesquisar(instituicao, pesquisa);
    }

    public DespesaBean buscarDespesa(Integer id) {
        return new DespesaJpaDao().findEntity(id);
    }

    public List<TipoAuxilioBean> listarTiposAuxilio(FinanciadorBean financiador) {
        return new TipoAuxilioJpaDao().pesquisar(financiador);
    }

    public List<TipoAuxilioBean> pesquisarTiposAuxilio(FinanciadorBean financiador, String pesquisa) {
        return new TipoAuxilioJpaDao().pesquisar(financiador, pesquisa);
    }

    public void cadastraCotacao(CotacaoBean cotacao) {
        new CotacaoJpaDao().create(cotacao);
    }

    public void atualizarCotacao(CotacaoBean cotacao) {
        new CotacaoJpaDao().update(cotacao);
    }

    public void excluirCotacao(CotacaoBean cotacao) {
        new CotacaoJpaDao().delete(cotacao);
    }

    public CotacaoBean buscarCotacao(Integer id) {
        return new CotacaoJpaDao().findEntity(id);
    }

    public ItemBean buscarItem(Integer id) {
        return new ItemJpaDao().findEntity(id);
    }

    public void cadastraItem(ItemBean item) {
        new ItemJpaDao().create(item);
    }

    public void atualizarItem(ItemBean item) {
        new ItemJpaDao().update(item);
    }

    public void excluirItem(ItemBean item) {
        new ItemJpaDao().delete(item);
    }

    public List<GrupoFornecimentoBean> listarGruposFornecimento() {
        return new GrupoFornecimentoJpaDao().findEntities();
    }

    public List<GrupoFornecimentoBean> pesquisarGruposFornecimento(String termoPesquisa) {
        return new GrupoFornecimentoJpaDao().pesquisar(termoPesquisa);
    }

    public void excluirGruposFornecimento(GrupoFornecimentoBean grupo) {
        new GrupoFornecimentoJpaDao().delete(grupo);
    }

    public String cadastraGruposFornecimento(GrupoFornecimentoBean grupo) {
        new GrupoFornecimentoJpaDao().create(grupo);
        return null;
    }

    public void atualizarGruposFornecimento(GrupoFornecimentoBean grupo) {
        new GrupoFornecimentoJpaDao().update(grupo);
    }

    public void excluirRepresentanteVendas(RepresentanteVendasBean representante) {
        new RepresentanteVendasJpaDao().delete(representante);
    }

    public String cadastrarRepresentanteVendas(RepresentanteVendasBean representante) {
        new RepresentanteVendasJpaDao().create(representante);
        return null;
    }

    public void atualizarRepresentanteVendas(RepresentanteVendasBean representante) {
        new RepresentanteVendasJpaDao().update(representante);
    }

    public FornecedorBean buscarFornecedor(Integer id) {
        return new FornecedorJpaDao().findEntity(id);
    }

    public GrupoFornecimentoBean buscarGrupoFornecimento(Integer id) {
        return new GrupoFornecimentoJpaDao().findEntity(id);
    }

    public RepresentanteVendasBean buscarRepresentanteVendas(Integer id) {
        return new RepresentanteVendasJpaDao().findEntity(id);
    }

    public void excluirCategoriaFornecedor(CategoriaFornecedorBean categoria) {
        new CategoriaFornecedorJpaDao().delete(categoria);
    }

    public String cadastrarCategoriaFornecedor(CategoriaFornecedorBean categoria) {
        new CategoriaFornecedorJpaDao().create(categoria);
        return null;
    }

    public void atualizarCategoriaFornecedor(CategoriaFornecedorBean categoria) {
        new CategoriaFornecedorJpaDao().update(categoria);
    }

    public List<CategoriaFornecedorBean> listaCategoriasFornecedores() {
        return new CategoriaFornecedorJpaDao().findEntities();
    }

    public List<CategoriaFornecedorBean> pesquisarCategoriasFornecedores(String pesquisa, GrupoFornecimentoBean grupo) {
        return new CategoriaFornecedorJpaDao().pesquisarCriteria(pesquisa, grupo);
    }

    public List<ProdutoBean> pesquisarProdutosCriteria(String termoPesquisa, GrupoFornecimentoBean grupo) {
        return new ProdutoJpaDao().pesquisarCriteria(termoPesquisa, grupo);
    }

    public List<FornecedorBean> pesquisarFornecedorCriteria(String termoPesquisa, String status) {
        return new FornecedorJpaDao().pesquisarCriteria(termoPesquisa, status);
    }

    public String cadastrarSolicitacaoCotacao(SolicitacaoCotacaoBean solicitacao) {
        new SolicitacaoCotacaoJpaDao().create(solicitacao);
        return null;
    }

    public void atualizarSolicitacaoCotacao(SolicitacaoCotacaoBean solicitacao) {
        new SolicitacaoCotacaoJpaDao().update(solicitacao);
    }

    public List<SolicitacaoCotacaoBean> listarSolicitacaoCotacao() {
        return new SolicitacaoCotacaoJpaDao().findEntities();
    }

    public SolicitacaoCotacaoBean buscarSolicitacao(Integer id) {
        return new SolicitacaoCotacaoJpaDao().findEntity(id);
    }

    public void cadastrarSolicitaOrcamento(SolicitaOrcamentoBean orcamento) {
        new SolicitaOrcamentoJpaDao().create(orcamento);
    }

    public SolicitaOrcamentoBean buscarSolicitacao(String chave) {
        return new SolicitaOrcamentoJpaDao().pesquisarSolicitaOrcamento(chave);
    }

    public SolicitaOrcamentoBean buscarSolicitacaoOrcamento(Integer id) {
        return new SolicitaOrcamentoJpaDao().findEntity(id);
    }

    public void atualizarSolicitacaoOrcametno(SolicitaOrcamentoBean solicitacao) {
        new SolicitaOrcamentoJpaDao().update(solicitacao);
    }

    public void excluirOrcamento(OrcamentoBean orcamento) {
        new OrcamentoJpaDao().delete(orcamento);
    }

    public void cadastrarOrcamento(OrcamentoBean orcamento) {
        new OrcamentoJpaDao().create(orcamento);
    }

    public void atualizarOrcamento(OrcamentoBean orcamento) {
        new OrcamentoJpaDao().update(orcamento);
    }

    public UnidadeBean buscarUnidade(Integer id) {
        return new UnidadeJpaDao().findEntity(id);
    }

    public void atualizarBolsaAlocada(BolsaAlocadaBean bolsa) {
        new BolsaAlocadaJpaDao().update(bolsa);
    }

    public BolsaAlocadaBean cadastrarBolsaAlocada(BolsaAlocadaBean bolsa) {
        return new BolsaAlocadaJpaDao().create(bolsa);
    }

    public void excluirBolsaAlocada(BolsaAlocadaBean bolsa) {
        new BolsaAlocadaJpaDao().delete(bolsa);
    }

    public List<DespesaBean> listarDespesaBolsa(BolsaAlocadaBean bolsa) {
        return new DespesaJpaDao().pesquisarBolsaAlocada(bolsa);
    }

    public OrcamentoBean buscarOrcamento(Integer id) {
        return new OrcamentoJpaDao().findEntity(id);
    }

    public List<PessoaBean> pesquisarPessoasAlunos() {
        PapelBean papel = new PapelJpaDao().findEntity(25);//25 Id do aluno grad
        PapelBean papel2 = new PapelJpaDao().findEntity(23);//23 Id do aluno pos
        List<PapelBean> listaPapeis = new PapelJpaDao().pesquisar("ALUNO");
        //23 Id do aluno pos
        List<PessoaBean> pessoas = new ArrayList<PessoaBean>();//papel.getPessoas();
        if (listaPapeis != null) {
            for (PapelBean pap : listaPapeis) {
                pessoas.addAll(pap.getPessoas());
            }
        }

        Collections.sort(pessoas, new Comparator() {
            public int compare(Object o1, Object o2) {
                PessoaBean p1 = (PessoaBean) o1;
                PessoaBean p2 = (PessoaBean) o2;
                return p1.getNome().compareTo(p2.getNome());
            }
        });

        return pessoas;
    }

    public List<ModalidadeBean> pesquisarModalidade(FinanciadorBean financiador, String sigla) {
        return new ModalidadeJpaDao().pesquisar(financiador, sigla);
    }

    public PessoaBean pesquisarPessoaNome(String username) {
        return new PessoaJpaDao().findByNome(username);
    }

    public void excluirCheque(DocumentoComprovacaoPagamentoBean documentoPagto) {
        new DocumentoComprovacaoPagamentoJpaDao().delete(documentoPagto);
    }

    public void excluirDocumentoFiscal(DocumentoComprovacaoDespesaBean documentoDespesa) {
        new DocumentoComprovacaoDespesaJpaDao().delete(documentoDespesa);
    }

    public void atualizarCotaBolsa(CotaBolsaBean cotaBolsa) {
        new CotaBolsaJpaDao().update(cotaBolsa);
    }

    public void excluirCotaBolsa(CotaBolsaBean cotaBolsa) {
        new CotaBolsaJpaDao().delete(cotaBolsa);
    }

    public DepartamentoBean buscarDepartamento(String instituicao, String unidade, String departamento) {
        List<DepartamentoBean> listaDepts = new DepartamentoJpaDao().pesquisarPorSigla(instituicao, unidade, departamento);
        if (listaDepts != null && !listaDepts.isEmpty()) {
            return listaDepts.get(0);
        } else {
            return null;
        }
    }

    public List<UnidadeBean> buscarUnidade(String sigla) {
        return new UnidadeJpaDao().pesquisar(sigla);
    }

    public List<CentroDespesaBean> pesquisarCentrosDespesa(String termoPesquisa, UnidadeBean unidade, CentroDespesaBean centroPai) {
        return new CentroDespesaJpaDao().pesquisar(termoPesquisa, unidade, centroPai);
    }

    public List<AuxilioBean> pesquisarNumeroMercurio(String numeroMercurio) {
        return new AuxilioJpaDao().pesquisarNumeroMercurio(numeroMercurio);
    }

    public List<AuxilioBean> pesquisarProtocolo(String protocolo) {
        return new AuxilioJpaDao().pesquisarProtocolo(protocolo);
    }

    public List<AuxilioBean> pesquisarProtocoloFinanciador(String protocoloFinanciador) {
        return new AuxilioJpaDao().pesquisarProtocoloFinanciador(protocoloFinanciador);
    }

    public void atualizarStatusAuxilio(AuxilioBean auxilio) {

        Calendar calendar=Calendar.getInstance();
        calendar.setTime(auxilio.getDataFinal());
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        auxilio.setDataFinal(calendar.getTime());

        Date agora = new Date();

        if (auxilio.getId() == null
                || auxilio.getStatus() == null
                || agora.before(auxilio.getDataInicial())) {
            auxilio.setStatus(AuxilioBean.STATUS_EM_ELABORACAO);
        } else if (AuxilioBean.STATUS_EM_ELABORACAO.equalsIgnoreCase(auxilio.getStatus())
                && agora.after(auxilio.getDataFinal())) {
            auxilio.setStatus(AuxilioBean.STATUS_FINALIZADO);
        } else if (AuxilioBean.STATUS_EM_ELABORACAO.equalsIgnoreCase(auxilio.getStatus())
                || AuxilioBean.STATUS_FINALIZADO.equalsIgnoreCase(auxilio.getStatus())){
            if(agora.before(auxilio.getDataFinal())
                    && agora.after(auxilio.getDataInicial())) {
                auxilio.setStatus(AuxilioBean.STATUS_EM_ANDAMENTO);
            }
        } else if(agora.after(auxilio.getDataFinal())){
            auxilio.setStatus(AuxilioBean.STATUS_FINALIZADO);
        }
    }

    public void atualizarStatusProjeto(ProjetoBean projeto) {
        if(projeto.getId() == null
                || projeto.getStatus() == null
                || projeto.getAuxilios().isEmpty()
                || new Date().before(projeto.getDataInicial())){
            projeto.setStatus(ProjetoBean.STATUS_EM_PLANEJAMENTO);
        }else if (ProjetoBean.STATUS_EM_PLANEJAMENTO.equalsIgnoreCase(projeto.getStatus())
                || ProjetoBean.STATUS_EM_ANDAMENTO.equalsIgnoreCase(projeto.getStatus())) {
            if (new Date().after(projeto.getDataFinal())) {
                projeto.setStatus(ProjetoBean.STATUS_ATRASADO);
            }
        }
    }

    public void atualizarStatusPrestacaoContas(PrestacaoContasBean prestacao) {
        if (!prestacao.getStatus().equals(PrestacaoContasBean.STATUS_CONCLUIDA) && !prestacao.getStatus().equals(PrestacaoContasBean.STATUS_ATRASADA)) {
            if (new Date().after(prestacao.getDataLimite())) {
                prestacao.setStatus(PrestacaoContasBean.STATUS_ATRASADA);
            }
        }
    }

    public void atualizarStatusRelatorioCientifico(RelatorioCientificoBean relatorio) {
        if (!relatorio.getStatus().equals(RelatorioCientificoBean.STATUS_ATRASADO) 
                && !relatorio.getStatus().equals(RelatorioCientificoBean.STATUS_ENTREGUE) 
                && !relatorio.getStatus().equals(RelatorioCientificoBean.STATUS_APROVADO) 
                && !relatorio.getStatus().equals(RelatorioCientificoBean.STATUS_NAO_APROVADO)) {
            if (new Date().after(relatorio.getDataLimite())) {
                relatorio.setStatus(RelatorioCientificoBean.STATUS_ATRASADO);
            }
        }
    }
    
    public List<DespesaBean> pesquisar(AuxilioBean aux, Date dataInicial, Date dataFinal) {
        return new DespesaJpaDao().pesquisar(aux, dataInicial, dataFinal);
    }
    
    public Date calcularDataInicialPrestacao(PrestacaoContasBean prestacao){
        Date retorno = prestacao.getFkAuxilio().getDataInicial();
        for(PrestacaoContasBean p : prestacao.getFkAuxilio().getPrestacaoContas()){
            if(p.getDataLimite().before(prestacao.getDataLimite()) && p.getDataLimite().after(retorno) && p!=prestacao){
                retorno = p.getDataLimite();
            }
        }
        return retorno;
    }
    
    public void registrarHistoricoAuxilio(HistoricoAuxilioBean historico){
        new HistoricoAuxilioJpaDao().create(historico);
    }
    
    public ModeloRelatorioPrestacaoContasBean buscarModeloRelatorio(Integer id){
        return new ModeloRelatorioPrestacaoContasJpaDao().findEntity(id);
    }
    
    public List<ModeloRelatorioPrestacaoContasBean> listarModelosRelatorios(){
        return new ModeloRelatorioPrestacaoContasJpaDao().findEntities();
    }
    
    public void setarModeloRelatorioDefault(ModeloRelatorioPrestacaoContasBean modelo){
        new ModeloRelatorioPrestacaoContasJpaDao().setarDefault(modelo);
    }
    
    public ModeloRelatorioPrestacaoContasBean getModeloRelatorioDefault(){
        return new ModeloRelatorioPrestacaoContasJpaDao().getDefault();
    }

    public SubcentroDespesasBean buscarSubcentroDespesasBean(Integer id) {
        return new SubcentroDespesasJpaDao().findEntity(id);
    }
    public PessoaProjetoBean buscarPessoaProjeto(Integer idPessoaProjeto) {
        return new PessoaProjetoJpaDao().findEntity(idPessoaProjeto);
    }
    public void excluirParticipanteAuxilio(ParticipanteAuxilioBean participante){
        new ParticipanteAuxilioJpaDao().delete(participante);
    }
    public CentroDespesaBean buscarCentroDespesaPorEstruturaHierarquica(String pesquisa){
        return new CentroDespesaJpaDao().buscarPorEstruturaHierarquica(pesquisa);
    }
    public List<HistoricoAuxilioBean> buscarHistoricoAlteracoes(int first, int pageSize, AuxilioBean auxilio, Date dataInicial, Date dataFinal, String alinea, String subcentro, int operacao, PessoaBean responsavel, String acao){
        return new HistoricoAuxilioJpaDao().pesquisar(first, pageSize, auxilio, dataInicial, dataFinal, alinea, subcentro, operacao, responsavel, acao);
    }
    public List<String> getUniqueHistoryActions(){
        return new HistoricoAuxilioJpaDao().getUniqueActions();
    }
    public List<DocumentoComprovacaoDespesaBean> pesquisarCriteriaRegistrosDuplicados(Date dataEmissao, String numero, FornecedorBean fornecedor){
        return new DocumentoComprovacaoDespesaJpaDao().pesquisarCriteriaRegistrosDuplicados(dataEmissao, numero, fornecedor);
    }

    public List<PessoaProjetoBean> buscarPessoasProjeto(ProjetoBean projeto) {
        return new PessoaProjetoJpaDao().findByProjeto(projeto);
    }
    
    public List<PessoaProjetoBean> buscarPessoasProjeto(PessoaBean pessoa) {
        return new PessoaProjetoJpaDao().findByPessoa(pessoa);
    }
    
    public void cadastraPessoaProjeto(PessoaProjetoBean pessoaProjetoBean){
        new PessoaProjetoJpaDao().create(pessoaProjetoBean);
    }
    public List<AlineaBean> buscarAlineasAuxilio(AuxilioBean aux){
        return new AlineaJpaDao().buscarPorAuxilio(aux);
    }
}
