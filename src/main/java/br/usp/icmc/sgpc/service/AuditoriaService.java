/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.service;

import br.usp.icmc.sgpc.beans.AuditoriaBean;
import br.usp.icmc.sgpc.beans.ConfiguracaoBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.dao.AuditoriaJpaDao;
import java.util.Date;
import java.util.TimeZone;
import org.apache.log4j.Logger;

/**
 *
 * @author herick
 */
public class AuditoriaService {

    private static AuditoriaService instance = new AuditoriaService();
    Logger logger = Logger.getLogger(this.getClass());

    private AuditoriaService() {
    }

    public static AuditoriaService getInstance() {
        return instance;
    }

    /**
     * Método deve ser utilizado quando um usuário está logado no sistema
     */
    public void gravarAcaoUsuario(PessoaBean pessoa, String acao, String modulo, String configAuditoria) {
        int id = pessoa.getId();
        String nomePessoa = pessoa.getNome();
        String username = pessoa.getUsername();

        gravarAcao(acao, modulo, id, nomePessoa, username, "", configAuditoria);
    }

    public void gravarAcaoUsuario(PessoaBean pessoa, String acao, String modulo, String descricao, String configAuditoria) {
        int id = pessoa.getId();
        String nomePessoa = pessoa.getNome();
        String username = pessoa.getUsername();

        gravarAcao(acao, modulo, id, nomePessoa, username, descricao, configAuditoria);
    }

    public void gravarAcaoSistema(String acao, String modulo, int idPessoa, String nomePessoa, String username, String configAuditoria) {
        gravarAcao(acao, modulo, idPessoa, nomePessoa, username, "", configAuditoria);
    }

    public void gravarAcaoSistema(String acao, String modulo, int idPessoa, String nomePessoa, String username, String descricao, String configAuditoria) {
        gravarAcao(acao, modulo, idPessoa, nomePessoa, username, descricao, configAuditoria);
    }

    public void gravarAcao(String acao, String modulo, int idPessoa, String nomePessoa, String username, String descricao, String configAuditoria) {

        ConfiguracaoBean conf = ConfiguracaoService.getInstance().getConfiguracao(configAuditoria);
        Boolean result;
        if (conf == null) {
            result = true;
        } else {
            result = Boolean.parseBoolean(conf.getValor());
        }
        if (result) {
            AuditoriaBean auditoria = new AuditoriaBean();
            TimeZone tz = TimeZone.getDefault();
            Date data = new Date();
            logger.debug("Data auditoria --- > " + new Date(data.getTime() + tz.getRawOffset()));

            if (descricao == null) {
                descricao = "";
            }

            auditoria.setAcao(acao);
            auditoria.setModulo(modulo);
            auditoria.setDataAcao(new Date(data.getTime() + tz.getRawOffset()));
            auditoria.setIdPessoa(idPessoa);
            auditoria.setNomePessoa(nomePessoa);
            auditoria.setUsername(username);
            auditoria.setDescricao(descricao);

            new AuditoriaJpaDao().create(auditoria);
            logger.debug("Auditoria executada");
        } else {
            logger.debug("Auditoria não executada devido a configuracao.");
        }
    }
}
