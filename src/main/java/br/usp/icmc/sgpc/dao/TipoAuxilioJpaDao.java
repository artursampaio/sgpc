/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.FinanciadorBean;
import br.usp.icmc.sgpc.beans.TipoAuxilioBean;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author david
 */
public class TipoAuxilioJpaDao extends GenericJpaDao<TipoAuxilioBean>  {

    public TipoAuxilioJpaDao() {
        super(TipoAuxilioBean.class);
    }

    public List<TipoAuxilioBean> pesquisar(String termoPesquisa) {
        List<TipoAuxilioBean> tipoAuxilio = null;
        EntityManager em = getEntityManager();
        try {
            Query queryTipoAuxilioByKeyword = em.createNamedQuery("TipoAuxilioBean.findByKeyword");
            queryTipoAuxilioByKeyword.setParameter("keyword", "%" + termoPesquisa + "%");
            tipoAuxilio = queryTipoAuxilioByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Tipo de Financiamento não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }

        return tipoAuxilio;
    }

    public List<TipoAuxilioBean> pesquisar(FinanciadorBean financiador) {
        List<TipoAuxilioBean> tiposAuxilio = new ArrayList<TipoAuxilioBean>();
        EntityManager em = getEntityManager();

        try {
            Query queryTipoAuxFinanciador = em.createNamedQuery("TipoAuxilioBean.findByFinanciador");
            queryTipoAuxFinanciador.setParameter("financiador", financiador);
            tiposAuxilio = queryTipoAuxFinanciador.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Unidade não cadastrada");
        } catch (Exception e) {
            logger.error(e);
        }

        return tiposAuxilio;
    }

    public List<TipoAuxilioBean> pesquisar(FinanciadorBean financiador, String termoPesquisa) {
        List<TipoAuxilioBean> tiposAuxilio = new ArrayList<TipoAuxilioBean>();
        EntityManager em = getEntityManager();

        try {
            Query queryTipoAuxFinanciador = em.createNamedQuery("TipoAuxilioBean.findByTermos");
            queryTipoAuxFinanciador.setParameter("financiador", financiador);
            queryTipoAuxFinanciador.setParameter("keyword",  "%" + termoPesquisa + "%");
            tiposAuxilio = queryTipoAuxFinanciador.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Unidade não cadastrada");
        } catch (Exception e) {
            logger.error(e);
        }

        return tiposAuxilio;
    }

}
