/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.*;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Artur
 */
public class AuxilioJpaDao extends GenericJpaDao<AuxilioBean> {

    public AuxilioJpaDao() {
        super(AuxilioBean.class);
    }

    public List<AuxilioBean> pesquisar() {
        List<AuxilioBean> auxilios = null;
        EntityManager em = getEntityManager();
        try {
            Query queryAuxilioByKeyword = em.createNamedQuery("AuxilioBean.findAll");
            auxilios = queryAuxilioByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }

        return auxilios;
    }

    public List<AuxilioBean> pesquisarOwnerCriteria(PessoaBean usuarioLogado
            , String termoPesquisa, FinanciadorBean financiador, String status
            , Date dataInicial, Date dataFinal, int first, int pageSize
            , String sortField, String sortOrder, String numeroProcessoEntidade
            , Date vigenciaDataInicial, Date vigenciaDataFinal
            , DepartamentoBean departamento, String pesquisaModalidade
            , UnidadeBean unidade, PessoaBean responsavelAuxilio
            , PessoaBean responsavelProjeto
            , String tipoBolsaAuxilio
            , Date dataPesquisaInicioIniciadoEm
            , Date dataPesquisaFimIniciadoEm) {
        List<AuxilioBean> auxilios = null;
        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<AuxilioBean> criteria = builder.createQuery(AuxilioBean.class);
        Root<AuxilioBean> root = criteria.from(AuxilioBean.class);
        Join<AuxilioBean, ProjetoBean> projeto = root.join(AuxilioBean_.fkProjeto);
        Join<ProjetoBean, PessoaBean> pessoaResponsavelProjeto = projeto.join(ProjetoBean_.fkResponsavel);
        Join<AuxilioBean, PessoaBean> pessoaResponsavelAuxilio = root.join(AuxilioBean_.fkResponsavel);

        Join<AuxilioBean, ModalidadeBean> modalidade = root.join(AuxilioBean_.fkModalidade);
        Join<ModalidadeBean, TipoAuxilioBean> tipoAuxilio = modalidade.join(ModalidadeBean_.fkTipoAuxilio);
        Join<TipoAuxilioBean, FinanciadorBean> financiadorP = tipoAuxilio.join(TipoAuxilioBean_.fkFinanciador);
        
        Join<ProjetoBean, DepartamentoBean> deptJoin = projeto.join(ProjetoBean_.fkDepartamento);
        Join<DepartamentoBean, UnidadeBean> unidadeJoin = deptJoin.join(DepartamentoBean_.fkUnidade);
        Join<UnidadeBean, InstituicaoBean> instituicaoJoin = unidadeJoin.join(UnidadeBean_.fkInstituicao);

        List<Predicate> predicados = new ArrayList<Predicate>();
        
        if(tipoBolsaAuxilio != null){
            Predicate tipoBolAux = builder.equal(builder.upper(root.get(AuxilioBean_.tipo)), tipoBolsaAuxilio.toUpperCase());
            predicados.add(tipoBolAux);
        }

        if (usuarioLogado != null) {
            Predicate nomeResponsavelProjeto = builder.equal(projeto.get(ProjetoBean_.fkResponsavel), usuarioLogado);
            Predicate nomeResponsavelAuxilio = builder.equal(root.get(AuxilioBean_.fkResponsavel), usuarioLogado);
            Predicate responsaveis = builder.or(nomeResponsavelProjeto,nomeResponsavelAuxilio);
            predicados.add(responsaveis);
        }
        if (!"".equals(termoPesquisa) && termoPesquisa != null) {
            Predicate observacao = builder.like(builder.upper(root.get(AuxilioBean_.observacoes)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nomeResponsavel = builder.like(builder.upper(pessoaResponsavelAuxilio.get(PessoaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate protocolo = builder.like(root.get(AuxilioBean_.protocolo), ("%" + termoPesquisa + "%"));
            Predicate numeroMercurio = builder.like(root.get(AuxilioBean_.numeroMercurio), ("%" + termoPesquisa + "%"));
            Predicate protocoloFinanciador = builder.like(root.get(AuxilioBean_.protocoloFinanciador), ("%" + termoPesquisa + "%"));
            Predicate nomeIdentificacao = builder.like(builder.upper(root.get(AuxilioBean_.nomeIdentificacao)), ("%" + termoPesquisa + "%").toUpperCase());

            Predicate soma = builder.or(observacao, nomeResponsavel, protocolo, numeroMercurio, protocoloFinanciador, nomeIdentificacao);
            predicados.add(soma);
        }
        if (financiador != null) {
            Predicate finan = builder.equal(tipoAuxilio.get(TipoAuxilioBean_.fkFinanciador), financiador);
            predicados.add(finan);
        }
        if (!"".equals(status) && status != null) {
            Predicate statusP = builder.equal(root.get(AuxilioBean_.status), status);
            predicados.add(statusP);
        }
        if (dataInicial != null) {
            Predicate dataIP = builder.greaterThanOrEqualTo(root.get(AuxilioBean_.dataInicial), dataInicial);
            predicados.add(dataIP);
        }
        if (dataFinal != null) {
            Predicate dataFP = builder.lessThanOrEqualTo(root.get(AuxilioBean_.dataFinal), dataFinal);
            predicados.add(dataFP);
        }
        if (vigenciaDataInicial != null && vigenciaDataFinal != null) {
            Predicate filtroDt1 = builder.lessThanOrEqualTo(root.get(AuxilioBean_.dataInicial), vigenciaDataInicial);
            Predicate filtroDt2 = builder.greaterThanOrEqualTo(root.get(AuxilioBean_.dataFinal), vigenciaDataInicial);
            Predicate parametroInicio = builder.and(filtroDt1, filtroDt2);
            
            Predicate filtroDt3 = builder.lessThanOrEqualTo(root.get(AuxilioBean_.dataInicial), vigenciaDataFinal);
            Predicate filtroDt4 = builder.greaterThanOrEqualTo(root.get(AuxilioBean_.dataFinal), vigenciaDataFinal);
            Predicate parametroFim = builder.and(filtroDt3, filtroDt4);
            
            Predicate filtroDt5 = builder.greaterThanOrEqualTo(root.get(AuxilioBean_.dataInicial), vigenciaDataInicial);
            Predicate filtroDt6 = builder.lessThanOrEqualTo(root.get(AuxilioBean_.dataFinal), vigenciaDataFinal);
            Predicate parametroEntre = builder.and(filtroDt5, filtroDt6);
            
            Predicate filtroDt7 = builder.lessThanOrEqualTo(root.get(AuxilioBean_.dataInicial), vigenciaDataInicial);
            Predicate filtroDt8 = builder.greaterThanOrEqualTo(root.get(AuxilioBean_.dataFinal), vigenciaDataFinal);
            Predicate parametroMaior = builder.and(filtroDt7, filtroDt8);
            
            Predicate vigencia = builder.or(parametroInicio, parametroFim, parametroEntre, parametroMaior);
            
            predicados.add(vigencia);
        }
    
        if (!"".equals(numeroProcessoEntidade) && numeroProcessoEntidade != null) {
            Predicate processoEntidadeP = builder.equal(root.get(AuxilioBean_.protocoloFinanciador), numeroProcessoEntidade);
            predicados.add(processoEntidadeP);
        }
        
        if(departamento != null){
            Predicate departamentoP = builder.equal(projeto.get(ProjetoBean_.fkDepartamento), departamento);
            predicados.add(departamentoP);
        }
        
        if(unidade != null){
            Predicate unidadeP = builder.equal(deptJoin.get(DepartamentoBean_.fkUnidade),unidade);
            predicados.add(unidadeP);
        }

        if(pesquisaModalidade != null){
            Predicate nomeModalidade = builder.like(builder.upper(modalidade.get(ModalidadeBean_.nome)),("%" + pesquisaModalidade + "%").toUpperCase());
            Predicate siglaModalidade = builder.like(builder.upper(modalidade.get(ModalidadeBean_.sigla)),("%" + pesquisaModalidade + "%").toUpperCase());
            
            Predicate soma = builder.or(nomeModalidade, siglaModalidade);
            predicados.add(soma);
        }
        
        if(responsavelAuxilio!=null){
            Predicate respAuxilio = builder.equal(root.get(AuxilioBean_.fkResponsavel), responsavelAuxilio);
            predicados.add(respAuxilio);
        }
        if(responsavelProjeto!=null){
            Predicate respProjeto = builder.equal(projeto.get(ProjetoBean_.fkResponsavel), responsavelProjeto);
            predicados.add(respProjeto);
        }
        //sql.append(" ORDER BY a.fkModalidade.fkTipoAuxilio.fkFinanciador.nome, a.numeroMercurio ");
        if(dataPesquisaInicioIniciadoEm!=null && dataPesquisaFimIniciadoEm!=null){
            Predicate dataIP = builder.greaterThanOrEqualTo(root.get(AuxilioBean_.dataInicial), dataPesquisaInicioIniciadoEm);
            Predicate dataFP = builder.lessThanOrEqualTo(root.get(AuxilioBean_.dataInicial), dataPesquisaFimIniciadoEm);
            Predicate composicaoDatas = builder.and(dataIP,dataFP);
            predicados.add(composicaoDatas);
        }

        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
        //criteria.orderBy(builder.asc(pessoaResponsavelProjeto.get(PessoaBean_.nome)), builder.asc(financiadorP.get(FinanciadorBean_.nome)), builder.asc(root.get(AuxilioBean_.numeroMercurio)));
        if(sortField!=null){
            Order ordenacao = null;
            
            Set<Join<AuxilioBean,?>> joins = root.getJoins();

            Path path = null;
            
            if("fkResponsavel.nome".equalsIgnoreCase(sortField)){
                path = pessoaResponsavelAuxilio.get(PessoaBean_.nome);
            }

            if("fkModalidade.nome".equalsIgnoreCase(sortField)){
                path = modalidade.get(ModalidadeBean_.nome);
            }

            if("fkModalidade.fkTipoAuxilio.fkFinanciador.sigla".equalsIgnoreCase(sortField)){
                path = financiadorP.get(FinanciadorBean_.sigla);
            }

            if(path == null){
                path = root.get(sortField);
            }
            

            if("ASCENDING".equalsIgnoreCase(sortOrder)){
                ordenacao = builder.asc(path);
            }
            if("DESCENDING".equalsIgnoreCase(sortOrder)){
                ordenacao = builder.desc(path);
            }
            if("UNSORTED".equalsIgnoreCase(sortOrder)){
                ordenacao = builder.asc(path);
            }
            if(ordenacao==null){
                ordenacao = builder.asc(path);
            }
            criteria.orderBy(ordenacao);
        }else{
            criteria.orderBy(builder.asc(pessoaResponsavelAuxilio.get(PessoaBean_.nome)), builder.asc(financiadorP.get(FinanciadorBean_.nome)), builder.asc(root.get(AuxilioBean_.numeroMercurio)));
        }

        Query queryAuxilioByOwner = em.createQuery(criteria);
        queryAuxilioByOwner.setFirstResult(first);
        if(pageSize>0){
            queryAuxilioByOwner.setMaxResults(pageSize);
        }
        auxilios = queryAuxilioByOwner.getResultList();

        return auxilios;
    }
    
    public List<AuxilioBean> pesquisarNumeroMercurio(String numeroMercurio) {
        List<AuxilioBean> auxilios = null;
        EntityManager em = getEntityManager();
        try {
            Query queryAuxilioByOwner = em.createNamedQuery("AuxilioBean.findByNumeroMercurio");
            queryAuxilioByOwner.setParameter("numeroMercurio", numeroMercurio);
            auxilios = queryAuxilioByOwner.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }
        return auxilios;
    }
    
    public List<AuxilioBean> pesquisarProtocolo(String protocolo) {
        List<AuxilioBean> auxilios = null;
        EntityManager em = getEntityManager();
        try {
            Query queryAuxilioByOwner = em.createNamedQuery("AuxilioBean.findByProtocolo");
            queryAuxilioByOwner.setParameter("protocolo", protocolo);
            auxilios = queryAuxilioByOwner.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }
        return auxilios;
    }
    
    public List<AuxilioBean> pesquisarProtocoloFinanciador(String protocoloFinanciador) {
        List<AuxilioBean> auxilios = null;
        EntityManager em = getEntityManager();
        try {
            Query queryAuxilioByOwner = em.createNamedQuery("AuxilioBean.findByProtocoloFinanciador");
            queryAuxilioByOwner.setParameter("protocoloFinanciador", protocoloFinanciador);
            auxilios = queryAuxilioByOwner.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }
        return auxilios;
    }
}
