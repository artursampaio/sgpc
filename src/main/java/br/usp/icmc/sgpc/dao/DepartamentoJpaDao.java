/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import br.usp.icmc.sgpc.beans.DepartamentoBean;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Artur
 */
public class DepartamentoJpaDao extends GenericJpaDao<DepartamentoBean> {

    public DepartamentoJpaDao() {
        super(DepartamentoBean.class);
    }

    public List<DepartamentoBean> pesquisar(String termoPesquisa) {
        List<DepartamentoBean> departamentos = null;
        EntityManager em = getEntityManager();
        try {
            Query queryDepartamentoByKeyword = em.createNamedQuery("DepartamentoBean.findByKeyword");
            queryDepartamentoByKeyword.setParameter("keyword", "%" + termoPesquisa + "%");
            departamentos = queryDepartamentoByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }

        return departamentos;
    }
    
    public List<DepartamentoBean> pesquisarPorSigla(String instituicao, String unidade, String departamento) {
        List<DepartamentoBean> departamentos = null;
        EntityManager em = getEntityManager();
        if(instituicao == null || unidade == null || departamento==null) return null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT o FROM DepartamentoBean o");
            sb.append(" WHERE (upper(o.sigla) LIKE upper(:departamento) OR upper(o.nome) LIKE upper(:departamento))");
            sb.append("   AND (upper(o.fkUnidade.sigla) LIKE upper(:unidade) OR upper(o.fkUnidade.nome) LIKE upper(:unidade))");
            sb.append("   AND (upper(o.fkUnidade.fkInstituicao.sigla) LIKE upper(:instituicao) OR upper(o.fkUnidade.fkInstituicao.nome) LIKE upper(:instituicao))");
            Query queryDepartamentoByKeyword = em.createQuery(sb.toString());
            queryDepartamentoByKeyword.setParameter("departamento", departamento);
            queryDepartamentoByKeyword.setParameter("unidade", unidade);
            queryDepartamentoByKeyword.setParameter("instituicao", instituicao);
            departamentos = queryDepartamentoByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }

        return departamentos;
    }

    public List<DepartamentoBean> pesquisarPorSigla(String sigla) {
        List<DepartamentoBean> departamentos = null;
        EntityManager em = getEntityManager();
        try {
            Query queryDepartamentoByKeyword = em.createNamedQuery("DepartamentoBean.findBySigla");
            queryDepartamentoByKeyword.setParameter("sigla", sigla);
            departamentos = queryDepartamentoByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }

        return departamentos;
    }

    public List<DepartamentoBean> pesquisar() {
        List<DepartamentoBean> departamentos = null;
        EntityManager em = getEntityManager();
        try {
            Query queryDepartamentoByKeyword = em.createNamedQuery("DepartamentoBean.findAll");
            departamentos = queryDepartamentoByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }
        
        return departamentos;
    }
}
