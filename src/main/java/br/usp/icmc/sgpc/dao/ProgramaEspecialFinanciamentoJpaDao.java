/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.ProgramaEspecialFinanciamentoBean;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
/**
 *
 * @author herick
 */
public class ProgramaEspecialFinanciamentoJpaDao extends GenericJpaDao<ProgramaEspecialFinanciamentoBean>{
    public ProgramaEspecialFinanciamentoJpaDao(){
        super(ProgramaEspecialFinanciamentoBean.class);
    }

    public List<ProgramaEspecialFinanciamentoBean> pesquisar(String termoPesquisa) {
        List<ProgramaEspecialFinanciamentoBean> programaEspecial = null;
        EntityManager em = getEntityManager();
        try {
            Query queryProgramaEspecialByKeyword = em.createNamedQuery("ProgramaEspecialFinanciamentoBean.findByKeyword");
            queryProgramaEspecialByKeyword.setParameter("keyword", "%" + termoPesquisa + "%");
            programaEspecial = queryProgramaEspecialByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Fornecedor não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }
        return programaEspecial;
    }
}
