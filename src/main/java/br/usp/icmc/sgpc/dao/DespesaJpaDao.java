/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.AlineaBean;
import br.usp.icmc.sgpc.beans.AlineaBean_;
import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.AuxilioBean_;
import br.usp.icmc.sgpc.beans.BolsaAlocadaBean;
import br.usp.icmc.sgpc.beans.CategoriaAlineaBean;
import br.usp.icmc.sgpc.beans.CategoriaAlineaBean_;
import br.usp.icmc.sgpc.beans.CentroDespesaBean;
import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.DespesaBean_;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoDespesaBean;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoDespesaBean_;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoPagamentoBean;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoPagamentoBean_;
import br.usp.icmc.sgpc.beans.FinanciadorBean;
import br.usp.icmc.sgpc.beans.FornecedorBean;
import br.usp.icmc.sgpc.beans.FornecedorBean_;
import br.usp.icmc.sgpc.beans.ModalidadeBean;
import br.usp.icmc.sgpc.beans.ModalidadeBean_;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PessoaBean_;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.beans.ProjetoBean_;
import br.usp.icmc.sgpc.beans.SubcentroDespesasBean;
import br.usp.icmc.sgpc.beans.SubcentroDespesasBean_;
import br.usp.icmc.sgpc.beans.TipoAuxilioBean;
import br.usp.icmc.sgpc.beans.TipoAuxilioBean_;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author ademilson
 */
public class DespesaJpaDao extends GenericJpaDao<DespesaBean> {

    public DespesaJpaDao() {
        super(DespesaBean.class);
    }

    public List<DespesaBean> pesquisar(String termoPesquisa) {

        List<DespesaBean> despesas = null;
        EntityManager em = getEntityManager();
        try {
            Query queryDespesaByKeyword = em.createNamedQuery("DespesaBean.findByKeyword");
            queryDespesaByKeyword.setParameter("keyword", "%" + termoPesquisa + "%");
            despesas = queryDespesaByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Despesa não cadastrada");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }
        return despesas;
    }

    public List<DespesaBean> pesquisar(AuxilioBean aux) {

        List<DespesaBean> despesas = new ArrayList<DespesaBean>();
        EntityManager em = getEntityManager();

        try {            
            Query queryDespesaByKeyword = em.createNamedQuery("DespesaBean.findByAuxilio");
            queryDespesaByKeyword.setParameter("auxilio", aux);
            despesas = queryDespesaByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Despesa não cadastrada");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }
        return despesas;
    }

    public List<DespesaBean> pesquisarOwner(PessoaBean usuarioLogado) {
        List<DespesaBean> despesas = null;
        EntityManager em = getEntityManager();
        try {            
            Query queryDespesaByOwner = em.createNamedQuery("DespesaBean.findByOwner");
            queryDespesaByOwner.setParameter("responsavel", usuarioLogado);
            despesas = queryDespesaByOwner.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }
        return despesas;
    }

    public List<DespesaBean> pesquisarBolsaAlocada(BolsaAlocadaBean bolsa) {
        List<DespesaBean> despesas = null;
        EntityManager em = getEntityManager();
        try {
            Query queryDespesaByOwner = em.createNamedQuery("DespesaBean.findByBolsa");
            queryDespesaByOwner.setParameter("bolsa", bolsa);
            despesas = queryDespesaByOwner.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }
        return despesas;
    }

    public List<DespesaBean> pesquisarOwnerCriteria(PessoaBean usuarioLogado, String termoPesquisa, Date dataRealizada,
            FinanciadorBean financiador, String status, CentroDespesaBean centroDespesa, AuxilioBean auxilio, AlineaBean alinea,
            String tipoDespesa, Date dataInicialDocFiscal, Date dataFinalDocFiscal, BigDecimal valorPesquisa, Date dataInicialCheque,
            Date dataFinalCheque, String numeroCheque, String numeroDocFiscal) {
        List<DespesaBean> despesas = null;

        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<DespesaBean> criteria = builder.createQuery(DespesaBean.class);
        Root<DespesaBean> root = criteria.from(DespesaBean.class);
        Join<DespesaBean, AlineaBean> alineaP = root.join(DespesaBean_.fkAlinea);
        Join<AlineaBean, AuxilioBean> auxilioP = alineaP.join(AlineaBean_.fkAuxilio);
        Join<AuxilioBean, ProjetoBean> projetoP = auxilioP.join(AuxilioBean_.fkProjeto);
        Join<ProjetoBean, PessoaBean> pessoaP = projetoP.join(ProjetoBean_.fkResponsavel);

        Join<AlineaBean, CategoriaAlineaBean> categoriaP = alineaP.join(AlineaBean_.fkCategoriaAlinea);

        Join<AuxilioBean, ModalidadeBean> modalidadeP = auxilioP.join(AuxilioBean_.fkModalidade);
        Join<ModalidadeBean, TipoAuxilioBean> tipoAuxP = modalidadeP.join(ModalidadeBean_.fkTipoAuxilio);
        
        Join<DespesaBean, FornecedorBean> fornecedorP = root.join(DespesaBean_.fkFornecedor, JoinType.LEFT);
        
        Join<DespesaBean, SubcentroDespesasBean> subcentroP = root.join(DespesaBean_.fkSubcentro);
        
        Join<DespesaBean, DocumentoComprovacaoDespesaBean> doccdP = root.join(DespesaBean_.documentosComprovacaoDespesas, JoinType.LEFT);
        Join<DespesaBean, DocumentoComprovacaoPagamentoBean> doccpP = root.join(DespesaBean_.documentosComprovacaoPagamentos, JoinType.LEFT);

        List<Predicate> predicados = new ArrayList<Predicate>();

        if (usuarioLogado != null) {
            Predicate nomeResponsavel = builder.equal(projetoP.get(ProjetoBean_.fkResponsavel), usuarioLogado);
            predicados.add(nomeResponsavel);
        }
        if (!"".equals(termoPesquisa) && termoPesquisa != null) {
            Predicate observacao = builder.like(builder.upper(root.get(DespesaBean_.observacao)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nomeResponsavel = builder.like(builder.upper(pessoaP.get(PessoaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nome = builder.like(builder.upper(categoriaP.get(CategoriaAlineaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate descricao = builder.like(builder.upper(root.get(DespesaBean_.descricao)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nomeFavorecido = builder.like(builder.upper(root.get(DespesaBean_.favorecidoNome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate cpfFavorecido = builder.like(builder.upper(root.get(DespesaBean_.favorecidoCpf)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nomeFantasiaFornecedor = builder.like(builder.upper(fornecedorP.get(FornecedorBean_.nomeFantasia)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate razaoSocialFornecedor = builder.like(builder.upper(fornecedorP.get(FornecedorBean_.razaoSocial)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate cnpjFornecedor = builder.like(builder.upper(fornecedorP.get(FornecedorBean_.cnpj)), ("%" + termoPesquisa + "%").toUpperCase());

            Predicate soma = builder.or(observacao, nomeResponsavel, nome, descricao, nomeFavorecido, cpfFavorecido, nomeFantasiaFornecedor, razaoSocialFornecedor, cnpjFornecedor);
            predicados.add(soma);
        }
        
        if(valorPesquisa!=null && valorPesquisa.doubleValue()!=0){
            Predicate valorP = builder.equal(root.get(DespesaBean_.valor), valorPesquisa);
            predicados.add(valorP);
        }

        if (dataRealizada != null) {
            Predicate dataR = builder.greaterThanOrEqualTo(root.get(DespesaBean_.dataRealizada), dataRealizada);
            predicados.add(dataR);
        }
        
        if(numeroDocFiscal!=null){
            Predicate numDocFiscal = builder.like(builder.upper(doccdP.get(DocumentoComprovacaoDespesaBean_.numero)), ("%" + numeroDocFiscal + "%").toUpperCase());
            predicados.add(numDocFiscal);
        }
        if (dataInicialDocFiscal != null) {
            Predicate datadf = builder.greaterThanOrEqualTo(doccdP.get(DocumentoComprovacaoDespesaBean_.dataEmissao), dataInicialDocFiscal);
            predicados.add(datadf);
        }
        if(dataFinalDocFiscal != null){
            Predicate datadf = builder.lessThanOrEqualTo(doccdP.get(DocumentoComprovacaoDespesaBean_.dataEmissao), dataFinalDocFiscal);
            predicados.add(datadf);
        }
        
        if(numeroCheque!=null){
            Predicate numCheque = builder.like(builder.upper(doccpP.get(DocumentoComprovacaoPagamentoBean_.numero)), ("%" + numeroCheque + "%").toUpperCase());
            predicados.add(numCheque);
        }
        if(dataInicialCheque != null){
            Predicate datach = builder.greaterThanOrEqualTo(doccpP.get(DocumentoComprovacaoPagamentoBean_.dataEmissao), dataInicialCheque);
            predicados.add(datach);
        }
        if(dataFinalCheque != null){
            Predicate datach = builder.lessThanOrEqualTo(doccpP.get(DocumentoComprovacaoPagamentoBean_.dataEmissao), dataFinalCheque);
            predicados.add(datach);
        }
        

        if (financiador != null) {
            Predicate financ = builder.equal(tipoAuxP.get(TipoAuxilioBean_.fkFinanciador), financiador);
            predicados.add(financ);
        }

        if (!"".equals(status) && status != null) {
            Predicate statusP = builder.equal(root.get(DespesaBean_.status), status);
            predicados.add(statusP);
        }

        if (centroDespesa != null) {
            //Predicate centroD = builder.equal(root.get(DespesaBean_.fkCentroDespesa), centroDespesa);
            Predicate centroD = builder.equal(subcentroP.get(SubcentroDespesasBean_.fkCentroDespesa), centroDespesa);
            predicados.add(centroD);
        }

        if(auxilio !=null){
            Predicate auxD = builder.equal(alineaP.get(AlineaBean_.fkAuxilio), auxilio);
            predicados.add(auxD);
        }
        
        if(alinea !=null){
            Predicate alineaD = builder.equal(root.get(DespesaBean_.fkAlinea), alinea);
            predicados.add(alineaD);
        }

        if (!"".equals(tipoDespesa) && tipoDespesa != null) {
            Predicate tipoDesp = builder.like(builder.upper(root.get(DespesaBean_.tipoDespesa)), ("%" + tipoDespesa + "%").toUpperCase());

            predicados.add(tipoDesp);
        }

        //sql.append(" ORDER BY a.fkAlinea.fkAuxilio.fkProjeto.fkResponsavel.nome, a.status ");
        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
        //criteria.orderBy(builder.asc(pessoaP.get(PessoaBean_.nome)), builder.asc(root.get(DespesaBean_.status)));
        criteria.orderBy(builder.desc(root.get(DespesaBean_.dataRealizada)));
        criteria.distinct(true);

        Query queryDespesas = em.createQuery(criteria);
        despesas = queryDespesas.getResultList();
        
        return despesas;
    }
    
    public List<DespesaBean> pesquisar(AuxilioBean aux, Date dataInicial, Date dataFinal) {

        List<DespesaBean> despesas = new ArrayList<DespesaBean>();
        EntityManager em = getEntityManager();

        try {            
            Query queryDespesaByKeyword = em.createNamedQuery("DespesaBean.relatorioPrestacaoContas");
            queryDespesaByKeyword.setParameter("auxilio", aux);
            queryDespesaByKeyword.setParameter("dataInicialPesquisa", dataInicial);
            queryDespesaByKeyword.setParameter("dataFinalPesquisa", dataFinal);
            despesas = queryDespesaByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Despesa não cadastrada");
        } catch (Exception e) {
            logger.error(e);
        }
        return despesas;
    }
    
    public BigDecimal totalDespesaSubcentroPorAlinea(SubcentroDespesasBean subcentroDespesas){
        BigDecimal resultado = new BigDecimal(0);
        String query = "SELECT SUM(d.valor) FROM DespesaBean d WHERE d.fkAlinea = :alinea AND d.fkSubcentro = :subcentro";
        EntityManager em = getEntityManager();
        try{
            Query queryDespesasPorSubcentro = em.createQuery(query);
            queryDespesasPorSubcentro.setParameter("alinea", subcentroDespesas.getFkAlinea());
            queryDespesasPorSubcentro.setParameter("subcentro", subcentroDespesas);
            resultado = (BigDecimal) queryDespesasPorSubcentro.getSingleResult();
        }catch(Exception e){
            logger.debug(e);
        }
        return resultado;
    }
}
