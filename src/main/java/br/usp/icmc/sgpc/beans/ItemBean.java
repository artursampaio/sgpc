/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "item")
@NamedQueries({
    @NamedQuery(name = "ItemBean.findAll", query = "SELECT i FROM ItemBean i"),
    @NamedQuery(name = "ItemBean.findById", query = "SELECT i FROM ItemBean i WHERE i.id = :id"),
    @NamedQuery(name = "ItemBean.findByQuantidade", query = "SELECT i FROM ItemBean i WHERE i.quantidade = :quantidade"),
    @NamedQuery(name = "ItemBean.findByUnidade", query = "SELECT i FROM ItemBean i WHERE i.unidade = :unidade"),
    @NamedQuery(name = "ItemBean.findByObservacao", query = "SELECT i FROM ItemBean i WHERE i.observacao = :observacao"),
    @NamedQuery(name = "ItemBean.findByStatus", query = "SELECT i FROM ItemBean i WHERE i.status = :status")})
public class ItemBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "item_id_item_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_item", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "quantidade", nullable = false)
    private BigDecimal quantidade;

    @Basic(optional = false)
    @Column(name = "unidade", nullable = false, length = 10)
    private String unidade;

    @Basic(optional = false)
    @Column(name = "observacao", nullable = false, length = 100)
    private String observacao;

    @Basic(optional = false)
    @Column(name = "status", nullable = false, length = 20)
    private String status;

    @JoinColumn(name = "fk_cotacao", referencedColumnName = "id_cotacao", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CotacaoBean fkCotacao;

    @JoinColumn(name = "fk_produto", referencedColumnName = "id_produto", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ProdutoBean fkProduto;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "fk_item", referencedColumnName = "id_item", nullable = false)    
    private List<SolicitacaoCotacaoBean> solicitacoesCotacoes;

    @JoinColumn(name = "fk_despesa", referencedColumnName = "id_despesa")
    @ManyToOne(fetch = FetchType.LAZY)
    private DespesaBean fkDespesa;

    public ItemBean() {
        solicitacoesCotacoes = new ArrayList<SolicitacaoCotacaoBean>();
    }

    public ItemBean(Integer id) {
        this.id = id;
    }

    public ItemBean(Integer id, BigDecimal quantidade, String unidade, String observacao, String status) {
        this.id = id;
        this.quantidade = quantidade;
        this.unidade = unidade;
        this.observacao = observacao;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CotacaoBean getFkCotacao() {
        return fkCotacao;
    }

    public void setFkCotacao(CotacaoBean fkCotacao) {
        this.fkCotacao = fkCotacao;
    }

    public ProdutoBean getFkProduto() {
        return fkProduto;
    }

    public void setFkProduto(ProdutoBean fkProduto) {
        this.fkProduto = fkProduto;
    }

    public DespesaBean getFkDespesa() {
        return fkDespesa;
    }

    public void setFkDespesa(DespesaBean fkDespesa) {
        this.fkDespesa = fkDespesa;
    }

    public List<SolicitacaoCotacaoBean> getSolicitacoesCotacoes() {
        return solicitacoesCotacoes;
    }

    public void setSolicitacoesCotacoes(List<SolicitacaoCotacaoBean> solicitacoesCotacoes) {
        this.solicitacoesCotacoes = solicitacoesCotacoes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemBean)) {
            return false;
        }
        ItemBean other = (ItemBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.Item[id=" + id + "]";
    }

}
