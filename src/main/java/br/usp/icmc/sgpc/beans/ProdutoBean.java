/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author david
 */
@Entity
@Table(name = "produto")
@NamedQueries({
    @NamedQuery(name = "ProdutoBean.findAll", query = "SELECT i FROM ProdutoBean i"),
    @NamedQuery(name = "ProdutoBean.findById", query = "SELECT i FROM ProdutoBean i WHERE i.id = :id"),
    //@NamedQuery(name = "ProdutoBean.findByNome", query = "SELECT i FROM ProdutoBean i WHERE i.nome = :nome"),
    @NamedQuery(name = "ProdutoBean.findByKeyword", query = "SELECT i FROM ProdutoBean i WHERE upper(i.descricao) LIKE upper(:keyword) OR upper(i.marca) LIKE upper(:keyword) OR upper(i.modelo) LIKE upper(:keyword)")})
public class ProdutoBean implements Serializable, SgpcBeanInterface {

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "produto_id_produto_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_produto", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "marca", nullable = false, length = 100)
    private String marca;

    @Basic(optional = false)
    @Column(name = "descricao", nullable = false, length = 1000)
    private String descricao;

    @Basic(optional = false)
    @Column(name = "modelo", nullable = false, length = 100)
    private String modelo;

    @JoinColumn(name = "fk_grupo_fornecimento", referencedColumnName = "id_grupo_fornecimento", nullable = false)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private GrupoFornecimentoBean fkGrupoFornecimento;

    public ProdutoBean() {
    }

    public GrupoFornecimentoBean getFkGrupoFornecimento() {
        return fkGrupoFornecimento;
    }

    public void setFkGrupoFornecimento(GrupoFornecimentoBean fkGrupoFornecimento) {
        this.fkGrupoFornecimento = fkGrupoFornecimento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}
