/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.CompromissoInterface;
import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "relatorio_cientifico")
@NamedQueries({
    @NamedQuery(name = "RelatorioCientificoBean.findAll", query = "SELECT r FROM RelatorioCientificoBean r"),
    @NamedQuery(name = "RelatorioCientificoBean.findById", query = "SELECT r FROM RelatorioCientificoBean r WHERE r.id = :id"),
    @NamedQuery(name = "RelatorioCientificoBean.findByDataLimite", query = "SELECT r FROM RelatorioCientificoBean r WHERE r.dataLimite = :dataLimite"),
    @NamedQuery(name = "RelatorioCientificoBean.findByDataLimiteEPessoa", query = "SELECT r FROM RelatorioCientificoBean r WHERE r.dataLimite = :dataLimite AND r.fkAuxilio.fkProjeto.fkResponsavel = :responsavel  ORDER BY r.fkAuxilio.fkModalidade.fkTipoAuxilio.fkFinanciador.nome"),
    @NamedQuery(name = "RelatorioCientificoBean.findByDataPeriodo", query = "SELECT r FROM RelatorioCientificoBean r WHERE r.dataLimite BETWEEN :dataInicial AND :dataFinal"),
    @NamedQuery(name = "RelatorioCientificoBean.findByStatus", query = "SELECT r FROM RelatorioCientificoBean r WHERE r.status = :status"),
    @NamedQuery(name = "RelatorioCientificoBean.findByDataPeriodoEPessoa", query = "SELECT r FROM RelatorioCientificoBean r WHERE r.dataLimite BETWEEN :dataInicial AND :dataFinal AND r.fkAuxilio.fkProjeto.fkResponsavel = :responsavel ORDER BY r.dataLimite")})
public class RelatorioCientificoBean implements Serializable, CompromissoInterface, SgpcBeanInterface {

    private static final long serialVersionUID = 1L;
    public static final String STATUS_ATRASADO = "Atrasado";
    public static final String STATUS_ENTREGUE = "Entregue";
    public static final String STATUS_APROVADO = "Aprovado";
    public static final String STATUS_NAO_APROVADO = "Não Aprovado";
    public static final String STATUS_AGENDADO = "Agendado";
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "relatorio_cientifico_id_relatorio_cientifico_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_relatorio_cientifico", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "data_limite", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataLimite;
    @Basic(optional = false)
    @Column(name = "status", nullable = false, length = 20)
    private String status;
    @Column(name = "descricao", length = 1000)
    private String descricao;
    @JoinColumn(name = "fk_auxilio", referencedColumnName = "id_auxilio", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AuxilioBean fkAuxilio;

    public RelatorioCientificoBean() {
    }

    public RelatorioCientificoBean(Integer id) {
        this.id = id;
    }

    public RelatorioCientificoBean(Integer id, Date dataLimite, String status) {
        this.id = id;
        this.dataLimite = dataLimite;
        this.status = status;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataLimite() {
        return dataLimite;
    }

    public void setDataLimite(Date dataLimite) {
        this.dataLimite = dataLimite;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RelatorioCientificoBean)) {
            return false;
        }
        RelatorioCientificoBean other = (RelatorioCientificoBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.RelatorioCientifico[id=" + id + "]";
    }

    public AuxilioBean getFkAuxilio() {
        return fkAuxilio;
    }

    public void setFkAuxilio(AuxilioBean fkAuxilio) {
        this.fkAuxilio = fkAuxilio;
    }
}
