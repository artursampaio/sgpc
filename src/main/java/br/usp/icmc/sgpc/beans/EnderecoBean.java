/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "endereco")
@NamedQueries({
    @NamedQuery(name = "EnderecoBean.findAll", query = "SELECT e FROM EnderecoBean e"),
    @NamedQuery(name = "EnderecoBean.findById", query = "SELECT e FROM EnderecoBean e WHERE e.id = :id"),
    @NamedQuery(name = "EnderecoBean.findByRua", query = "SELECT e FROM EnderecoBean e WHERE e.rua = :rua"),
    @NamedQuery(name = "EnderecoBean.findByNumero", query = "SELECT e FROM EnderecoBean e WHERE e.numero = :numero"),
    @NamedQuery(name = "EnderecoBean.findByComplemento", query = "SELECT e FROM EnderecoBean e WHERE e.complemento = :complemento"),
    @NamedQuery(name = "EnderecoBean.findByBairro", query = "SELECT e FROM EnderecoBean e WHERE e.bairro = :bairro"),
    @NamedQuery(name = "EnderecoBean.findByCep", query = "SELECT e FROM EnderecoBean e WHERE e.cep = :cep")})
public class EnderecoBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="endereco_id_endereco_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_endereco", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "rua", nullable = false, length = 200)
    private String rua;

    @Basic(optional = false)
    @Column(name = "numero", nullable = false, length = 20)
    private int numero;

    @Column(name = "complemento", length = 200)
    private String complemento;

    @Basic(optional = false)
    @Column(name = "bairro", nullable = false, length = 100)
    private String bairro;

    @Basic(optional = false)
    @Column(name = "cep", nullable = false, length = 20)
    private String cep;

    @JoinColumn(name = "fk_cities", referencedColumnName = "id_city", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MunicipioBean fkMunicipio;

    @OneToOne(mappedBy="fkEndereco",fetch = FetchType.LAZY)
    private FornecedorBean fkFornecedor;
 
    @OneToOne(mappedBy="fkEndereco",fetch = FetchType.LAZY)
    private InstituicaoBean fkInstituicao;

    @OneToMany(mappedBy = "fkEndereco", fetch = FetchType.LAZY)
    private List<PessoaBean> pessoas;

    public EnderecoBean() {
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public FornecedorBean getFkFornecedor() {
        return fkFornecedor;
    }

    public void setFkFornecedor(FornecedorBean fkFornecedor) {
        this.fkFornecedor = fkFornecedor;
    }

    public InstituicaoBean getFkInstituicao() {
        return fkInstituicao;
    }

    public void setFkInstituicao(InstituicaoBean fkInstituicao) {
        this.fkInstituicao = fkInstituicao;
    }

    public MunicipioBean getFkMunicipio() {
        return fkMunicipio;
    }

    public void setFkMunicipio(MunicipioBean fkMunicipio) {
        this.fkMunicipio = fkMunicipio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public List<PessoaBean> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<PessoaBean> pessoas) {
        this.pessoas = pessoas;
    }
    
    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.Endereco[id=" + id + "]";
    }

}
