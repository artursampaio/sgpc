/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "historicoauxilio")
public class HistoricoAuxilioBean implements Serializable{
    private static final long serialVersionUID = 1L;
    public static final int OPERACAO_CREDITO = 1;
    public static final int OPERACAO_DEBITO = 2;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "historicoauxilio_id_historicoauxilio_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_historicoauxilio", nullable = false)
    private Integer id;
    
    @JoinColumn(name = "fk_auxilio", referencedColumnName = "id_auxilio", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected AuxilioBean fkAuxilio;
    
    @Column(name = "data_alteracao")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date dataAlteracao;
    
    @Column(name = "acao", nullable = false, length = 50)
    private String acao;
    
    @Column(name = "descricao", nullable = false, length = 250)
    private String descricao;
    
    @JoinColumn(name = "fk_responsavel", referencedColumnName = "id_pessoa", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private PessoaBean fkResponsavel;
    
    @Column(name = "nomeAlinea", nullable = false, length = 250)
    private String nomeAlinea;
    
    @Column(name = "valor")
    private BigDecimal valor;
    
    @Column(name = "operacao")
    private Integer operacao;
    
    @Column(name = "nome_centro_despesas", length = 250)
    private String nomeCentroDespesas;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_historico_editado")
    private List<HistoricoAuxilioBean> historicosAnteriores;
    
    @Column(name = "editado", nullable=false, columnDefinition="boolean default false")
    private boolean editado;
    
    public HistoricoAuxilioBean(){
        historicosAnteriores = new ArrayList<HistoricoAuxilioBean>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AuxilioBean getFkAuxilio() {
        return fkAuxilio;
    }

    public void setFkAuxilio(AuxilioBean fkAuxilio) {
        this.fkAuxilio = fkAuxilio;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public PessoaBean getFkResponsavel() {
        return fkResponsavel;
    }

    public void setFkResponsavel(PessoaBean fkResponsavel) {
        this.fkResponsavel = fkResponsavel;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoAuxilioBean)) {
            return false;
        }
        HistoricoAuxilioBean other = (HistoricoAuxilioBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public String getNomeAlinea() {
        return nomeAlinea;
    }

    public void setNomeAlinea(String nomeAlinea) {
        this.nomeAlinea = nomeAlinea;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Integer getOperacao() {
        return operacao;
    }

    public void setOperacao(Integer operacao) {
        this.operacao = operacao;
    }

    public String getNomeCentroDespesas() {
        return nomeCentroDespesas;
    }

    public void setNomeCentroDespesas(String nomeCentroDespesas) {
        this.nomeCentroDespesas = nomeCentroDespesas;
    }

    public List<HistoricoAuxilioBean> getHistoricosAnteriores() {
        return historicosAnteriores;
    }

    public void setHistoricosAnteriores(List<HistoricoAuxilioBean> historicosAnteriores) {
        this.historicosAnteriores = historicosAnteriores;
    }

    public boolean isEditado() {
        return editado;
    }

    public void setEditado(boolean editado) {
        this.editado = editado;
    }
    
}
