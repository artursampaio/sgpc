/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "organizacao_evento")
@PrimaryKeyJoinColumn (name = "id_projeto")
@NamedQueries({
    @NamedQuery(name = "EventoBean.findAll", query = "SELECT e FROM EventoBean e"),
    @NamedQuery(name = "EventoBean.findByKeyword", query = "SELECT e FROM EventoBean e WHERE upper(e.titulo) LIKE upper(:keyword) OR upper(e.descricao) LIKE upper(:keyword) ORDER BY e.dataCriacao desc, e.titulo "),
    @NamedQuery(name = "EventoBean.findByOwner", query = "SELECT e FROM EventoBean e WHERE e.fkResponsavel = :responsavel"),
    @NamedQuery(name = "EventoBean.findByOwnerTerm", query = "SELECT e FROM EventoBean e WHERE e.fkResponsavel = :responsavel AND (upper(e.titulo) LIKE upper(:keyword) OR upper(e.descricao) LIKE upper(:keyword))"),
    @NamedQuery(name = "EventoBean.findById", query = "SELECT e FROM EventoBean e WHERE e.id = :id")})

public class EventoBean extends ProjetoBean implements Serializable{
	
	private static final long serialVersionUID = 1L;

    @OneToMany(mappedBy = "fkEvento", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<AtividadeBean> atividades;

    public EventoBean() {
        atividades = new ArrayList<AtividadeBean>();
    }

    public List<AtividadeBean> getAtividades() {
        return atividades;
    }

    public void setAtividades(List<AtividadeBean> atividades) {
        this.atividades = atividades;
    }

    @Override
    public int getTipo() {
        return EVENTO;
    }
    
}
