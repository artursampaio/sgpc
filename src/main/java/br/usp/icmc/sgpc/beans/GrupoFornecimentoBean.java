/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "grupo_fornecimento")
@NamedQueries({
    @NamedQuery(name = "GrupoFornecimentoBean.findAll", query = "SELECT g FROM GrupoFornecimentoBean g"),
    @NamedQuery(name = "GrupoFornecimentoBean.findById", query = "SELECT g FROM GrupoFornecimentoBean g WHERE g.id = :id"),
    @NamedQuery(name = "GrupoFornecimentoBean.findByNome", query = "SELECT g FROM GrupoFornecimentoBean g WHERE g.nome = :nome"),
    @NamedQuery(name = "GrupoFornecimentoBean.findByKeyword", query = "SELECT g FROM GrupoFornecimentoBean g WHERE upper(g.descricao) LIKE upper(:keyword) OR upper(g.nome) LIKE upper(:keyword)"),
    @NamedQuery(name = "GrupoFornecimentoBean.findByDescricao", query = "SELECT g FROM GrupoFornecimentoBean g WHERE g.descricao = :descricao")})
public class GrupoFornecimentoBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "grupo_fornecimento_id_grupo_fornecimento_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_grupo_fornecimento", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 100)
    private String nome;

    @Basic(optional = false)
    @Column(name = "descricao", nullable = false, length = 1000)
    private String descricao;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkGrupoFornecimento", fetch = FetchType.LAZY)
    private List<SolicitacaoCotacaoBean> solicitacoes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkGrupoFornecimento", fetch = FetchType.LAZY)
    private List<ProdutoBean> produtos;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_grupo_fornecimento", referencedColumnName = "id_grupo_fornecimento")
    private List<CategoriaFornecedorBean> fornecedores;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_grupo_fornecimento", referencedColumnName = "id_grupo_fornecimento")
    private List<CategoriaFornecedorBean> categorias;

    public GrupoFornecimentoBean() {
    }

    public GrupoFornecimentoBean(Integer id) {
        this.id = id;
    }

    public List<CategoriaFornecedorBean> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CategoriaFornecedorBean> categorias) {
        this.categorias = categorias;
    }

    public GrupoFornecimentoBean(Integer id, String nome, String descricao) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<SolicitacaoCotacaoBean> getSolicitacoes() {
        return solicitacoes;
    }

    public void setSolicitacoes(List<SolicitacaoCotacaoBean> solicitacoes) {
        this.solicitacoes = solicitacoes;
    }

    public List<ProdutoBean> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<ProdutoBean> produtos) {
        this.produtos = produtos;
    }

    public List<CategoriaFornecedorBean> getFornecedores() {
        return fornecedores;
    }

    public void setFornecedores(List<CategoriaFornecedorBean> fornecedores) {
        this.fornecedores = fornecedores;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupoFornecimentoBean)) {
            return false;
        }
        GrupoFornecimentoBean other = (GrupoFornecimentoBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nome;
    }

}
