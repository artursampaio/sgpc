/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author david
 */
@Entity
@Table(name = "modalidade")
@NamedQueries({
    @NamedQuery(name = "ModalidadeBean.findAll", query = "SELECT a FROM ModalidadeBean a"),
    @NamedQuery(name = "ModalidadeBean.findByNome", query = "SELECT a FROM ModalidadeBean a WHERE a.nome = :nome"),
    @NamedQuery(name = "ModalidadeBean.findById", query = "SELECT a FROM ModalidadeBean a WHERE a.id = :id")})
public class ModalidadeBean implements Serializable, SgpcBeanInterface {
    
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "modalidade_id_modalidade_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_modalidade", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 100)
    private String nome;

    @Column(name = "sigla", length = 20)
    private String sigla;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_tipo_auxilio", referencedColumnName ="id_tipo_auxilio",  nullable = false)
    private TipoAuxilioBean fkTipoAuxilio;
    
    @Column(name = "permiteDespesaAntesVigencia", nullable=false, columnDefinition="boolean default false")
    private boolean permiteDespesaAntesVigencia;
    
    @Column(name = "permiteDespesaAposVigencia", nullable=false, columnDefinition="boolean default false")
    private boolean permiteDespesaAposVigencia;

    public ModalidadeBean() {
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public TipoAuxilioBean getFkTipoAuxilio() {
        return fkTipoAuxilio;
    }

    public void setFkTipoAuxilio(TipoAuxilioBean fkTipoAuxilio) {
        this.fkTipoAuxilio = fkTipoAuxilio;
    }

    public ModalidadeBean(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModalidadeBean)) {
            return false;
        }
        ModalidadeBean other = (ModalidadeBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getNome();
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean isPermiteDespesaAntesVigencia() {
        return permiteDespesaAntesVigencia;
    }

    public void setPermiteDespesaAntesVigencia(boolean permiteDespesaAntesVigencia) {
        this.permiteDespesaAntesVigencia = permiteDespesaAntesVigencia;
    }

    public boolean isPermiteDespesaAposVigencia() {
        return permiteDespesaAposVigencia;
    }

    public void setPermiteDespesaAposVigencia(boolean permiteDespesaAposVigencia) {
        this.permiteDespesaAposVigencia = permiteDespesaAposVigencia;
    }
    
}
