/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.CompromissoInterface;
import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "prestacao_contas")
@NamedQueries({
    @NamedQuery(name = "PrestacaoContasBean.findAll", query = "SELECT p FROM PrestacaoContasBean p"),
    @NamedQuery(name = "PrestacaoContasBean.findByKeyword", query = "SELECT p FROM PrestacaoContasBean p WHERE upper(p.descricao) LIKE upper(:keyword) OR upper(p.fkAuxilio.protocolo) LIKE upper(:keyword) OR upper(p.fkAuxilio.numeroMercurio) LIKE upper(:keyword) OR upper(p.fkAuxilio.protocoloFinanciador) LIKE upper(:keyword)"),
    @NamedQuery(name = "PrestacaoContasBean.findById", query = "SELECT p FROM PrestacaoContasBean p WHERE p.id = :id"),
    @NamedQuery(name = "PrestacaoContasBean.findByDataLimite", query = "SELECT p FROM PrestacaoContasBean p WHERE p.dataLimite = :dataLimite"),
    @NamedQuery(name = "PrestacaoContasBean.findByDataLimiteEPessoa", query = "SELECT p FROM PrestacaoContasBean p WHERE p.dataLimite = :dataLimite AND p.fkAuxilio.fkProjeto.fkResponsavel = :responsavel ORDER BY p.fkAuxilio.fkModalidade.fkTipoAuxilio.fkFinanciador.nome"),
    @NamedQuery(name = "PrestacaoContasBean.findByDataPeriodo", query = "SELECT p FROM PrestacaoContasBean p WHERE p.dataLimite BETWEEN :dataInicial AND :dataFinal ORDER BY p.dataLimite, p.fkAuxilio.id, p.fkAuxilio.fkProjeto.fkResponsavel.nome"),
    @NamedQuery(name = "PrestacaoContasBean.findByStatus", query = "SELECT p FROM PrestacaoContasBean p WHERE p.status = :status"),
    @NamedQuery(name = "PrestacaoContasBean.findByDataPeriodoEPessoa", query = "SELECT p FROM PrestacaoContasBean p WHERE p.dataLimite BETWEEN :dataInicial AND :dataFinal AND p.fkAuxilio.fkProjeto.fkResponsavel = :responsavel ORDER BY p.dataLimite")})
public class PrestacaoContasBean implements Serializable, CompromissoInterface, SgpcBeanInterface {

    private static final long serialVersionUID = 1L;
    public static final String STATUS_CONCLUIDA = "Concluída";
    public static final String STATUS_ATRASADA = "Atrasada";
    public static final String STATUS_AGENDADA = "Agendada";
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "prestacao_contas_id_prestacao_contas_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_prestacao_contas", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "data_limite", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataLimite;
    @Basic(optional = false)
    @Column(name = "status", nullable = false, length = 20)
    private String status;
    @OneToMany(mappedBy = "fkPrestacaoContas", fetch = FetchType.LAZY)
    private List<DespesaBean> despesas;
    @Column(name = "descricao", length = 1000)
    private String descricao;
    @JoinColumn(name = "fk_auxilio", referencedColumnName = "id_auxilio", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AuxilioBean fkAuxilio;
    @JoinColumn(name = "fk_responsavel", referencedColumnName = "id_pessoa", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private PessoaBean fkResponsavel;

    public PrestacaoContasBean() {
    }

    public PrestacaoContasBean(Integer id) {
        this.id = id;
    }

    public PrestacaoContasBean(Integer id, Date dataLimite, String status) {
        this.id = id;
        this.dataLimite = dataLimite;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public PessoaBean getFkResponsavel() {
        return fkResponsavel;
    }

    public void setFkResponsavel(PessoaBean fkResponsavel) {
        this.fkResponsavel = fkResponsavel;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataLimite() {
        return dataLimite;
    }

    public void setDataLimite(Date dataLimite) {
        this.dataLimite = dataLimite;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DespesaBean> getDespesas() {
        return despesas;
    }

    public void setDespesas(List<DespesaBean> despesas) {
        this.despesas = despesas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrestacaoContasBean)) {
            return false;
        }
        PrestacaoContasBean other = (PrestacaoContasBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getDescricao();
    }

    public AuxilioBean getFkAuxilio() {
        return fkAuxilio;
    }

    public void setFkAuxilio(AuxilioBean fkAuxilio) {
        this.fkAuxilio = fkAuxilio;
    }
}
