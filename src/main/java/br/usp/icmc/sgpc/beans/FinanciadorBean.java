/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "financiador")
@Inheritance(strategy=InheritanceType.JOINED)
@NamedQueries({
    @NamedQuery(name = "FinanciadorBean.findAll", query = "SELECT f FROM FinanciadorBean f"),
    @NamedQuery(name = "FinanciadorBean.findById", query = "SELECT f FROM FinanciadorBean f WHERE f.id = :id"),
    @NamedQuery(name = "FinanciadorBean.findByNome", query = "SELECT f FROM FinanciadorBean f WHERE f.nome = :nome"),
    @NamedQuery(name = "FinanciadorBean.findByCnpj", query = "SELECT f FROM FinanciadorBean f WHERE f.cnpj = :cnpj"),
    @NamedQuery(name = "FinanciadorBean.findByKeyword", query = "SELECT f FROM FinanciadorBean f WHERE upper(f.nome) LIKE upper(:keyword) OR upper(f.sigla) LIKE upper(:keyword) OR upper(f.cnpj) LIKE upper(:keyword) OR upper(f.email) LIKE upper(:keyword)")})
public class FinanciadorBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="financiador_id_financiador_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_financiador", nullable = false)
    protected Integer id;

    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 200)
    protected String nome;

    @Basic(optional = false)
    @Column(name = "sigla", nullable = false, length = 20)
    protected String sigla;

    @Basic(optional = false)
    @Column(name = "cnpj", nullable = false, length = 20)
    protected String cnpj;

    @Basic(optional = false)
    @Column(name = "email", nullable = false, length = 50)
    protected String email;

    @OneToMany(mappedBy = "fkFinanciador", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<TipoAuxilioBean> tiposAuxilio = new ArrayList<TipoAuxilioBean>();
    
    @JoinColumn(name = "fk_modelorelatorio", referencedColumnName = "id_modelorelatorio", nullable=true)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private ModeloRelatorioPrestacaoContasBean fkModeloRelatorio;

    public List<TipoAuxilioBean> getTiposAuxilio() {
        return tiposAuxilio;
    }

    public void setTiposAuxilio(List<TipoAuxilioBean> tiposAuxilio) {
        this.tiposAuxilio = tiposAuxilio;
    }

    public FinanciadorBean() {
    }

    public FinanciadorBean(Integer id) {
        this.id = id;
    }

    public FinanciadorBean(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ModeloRelatorioPrestacaoContasBean getFkModeloRelatorio() {
        return fkModeloRelatorio;
    }

    public void setFkModeloRelatorio(ModeloRelatorioPrestacaoContasBean fkModeloRelatorio) {
        this.fkModeloRelatorio = fkModeloRelatorio;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FinanciadorBean)) {
            return false;
        }
        FinanciadorBean other = (FinanciadorBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getSigla()+ " - " + this.getNome();
    }

}
