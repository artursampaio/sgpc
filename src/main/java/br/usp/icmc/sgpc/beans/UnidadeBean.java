/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "unidade")
@NamedQueries({
    @NamedQuery(name = "UnidadeBean.findAll", query = "SELECT u FROM UnidadeBean u ORDER BY u.nome"),
    @NamedQuery(name = "UnidadeBean.findById", query = "SELECT u FROM UnidadeBean u WHERE u.id = :id"),
    @NamedQuery(name = "UnidadeBean.findByNome", query = "SELECT u FROM UnidadeBean u WHERE u.nome = :nome"),
    @NamedQuery(name = "UnidadeBean.findBySigla", query = "SELECT u FROM UnidadeBean u WHERE upper(u.sigla) = upper(:sigla)"),
    @NamedQuery(name = "UnidadeBean.findByInstituicao", query = "SELECT u FROM UnidadeBean u WHERE u.fkInstituicao = :instituicao ORDER BY u.nome"),
    @NamedQuery(name = "UnidadeBean.findByTermos", query = "SELECT u FROM UnidadeBean u WHERE u.fkInstituicao = :instituicao AND upper(u.nome) LIKE upper(:keyword) OR upper(u.sigla) LIKE upper(:keyword) ORDER BY u.nome"),
    @NamedQuery(name = "UnidadeBean.findByKeyword", query = "SELECT u FROM UnidadeBean u WHERE upper(u.nome) LIKE upper(:keyword) OR upper(u.sigla) LIKE upper(:keyword)")})
public class UnidadeBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "unidade_id_unidade_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_unidade", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 100)
    protected String nome;

    @Basic(optional = false)
    @Column(name = "sigla", nullable = false, length = 100)
    protected String sigla;

    @JoinColumn(name = "fk_instituicao", referencedColumnName = "id_instituicao")
    @ManyToOne(fetch = FetchType.LAZY)
    private InstituicaoBean fkInstituicao;

    @OneToMany(mappedBy = "fkUnidade", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @OrderBy("sigla ASC")
    private List<DepartamentoBean> departamentos;
    
    @OneToMany(mappedBy = "fkUnidade", fetch = FetchType.LAZY)
    private List<CentroDespesaBean> centrosDespesas;

    public InstituicaoBean getFkInstituicao() {
        return fkInstituicao;
    }

    public void setFkInstituicao(InstituicaoBean fkInstituicao) {
        this.fkInstituicao = fkInstituicao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<DepartamentoBean> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(List<DepartamentoBean> departamentos) {
        this.departamentos = departamentos;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnidadeBean)) {
            return false;
        }
        UnidadeBean other = (UnidadeBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return this.getSigla();
    }

    public List<CentroDespesaBean> getCentrosDespesas() {
        return centrosDespesas;
    }

    public void setCentrosDespesas(List<CentroDespesaBean> centrosDespesas) {
        this.centrosDespesas = centrosDespesas;
    }
}
