/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.SolicitaOrcamentoBean;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 *
 * @author herick
 */
@ManagedBean(name = "licitacaoMB")
@ViewScoped
public class LicitacaoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(LicitacaoManagedBean.class);
    private String chaveAcesso;

    public LicitacaoManagedBean() {
    }

    public String getChaveAcesso() {
        return chaveAcesso;
    }

    public void setChaveAcesso(String chaveAcesso) {
        this.chaveAcesso = chaveAcesso;
    }

    public String acessarLicitacao() {
        if (isCamposValidos()) {
            try {
                SolicitaOrcamentoBean solicitaOrcamento = Service.getInstance().buscarSolicitacao(chaveAcesso);
                Date agora = new Date();
                FacesContext ctx = FacesContext.getCurrentInstance();
                Map sessionMap = ctx.getExternalContext().getSessionMap();
                UserSessionManagedBean userSessionMB = (UserSessionManagedBean) sessionMap.get("userSessionMB");
                if (userSessionMB == null) {
                    userSessionMB = new UserSessionManagedBean();
                    sessionMap.put("userSessionMB", userSessionMB);
                }
                userSessionMB.setInterPageParameter(solicitaOrcamento);

                if (solicitaOrcamento != null) {
                    if (solicitaOrcamento.getFkSolicitacaoCotacao().getDataLimiteResposta().before(agora)) {
                        FacesContext.getCurrentInstance().addMessage("invalido", new FacesMessage("Chave de Acesso Expirada"));
                        return "licitacao";
                    } else {
                        userSessionMB.setFornecedor(solicitaOrcamento.getFkFornecedor());
                        return "orcamento?faces-redirect=true";
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage("invalido", new FacesMessage("Chave de Acesso Inválida"));
                    return "licitacao";
                }
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage("invalido", new FacesMessage(e.getMessage()));
                return "licitacao";
            }
        } else {
            return "licitacao";
        }
    }

    private boolean isCamposValidos() {
        boolean camposValidos = true;
        if ("".equals(chaveAcesso)) {
            FacesContext.getCurrentInstance().addMessage("invalido", new FacesMessage("* O Campo Chave  deve ser preenchido."));
            camposValidos = false;
        }
        return camposValidos;
    }
}
