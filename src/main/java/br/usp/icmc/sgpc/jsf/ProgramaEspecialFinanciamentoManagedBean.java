/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.ProgramaEspecialFinanciamentoBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author herick
 */
@ManagedBean(name = "programaEspecialMB")
@ViewScoped
public class ProgramaEspecialFinanciamentoManagedBean implements Serializable {

    private ProgramaEspecialFinanciamentoBean programaEspecial;
    private String pesquisa;
    private static final Logger logger = Logger.getLogger(ProgramaEspecialFinanciamentoManagedBean.class);
    private List<ProgramaEspecialFinanciamentoBean> listaProgramasEspeciais = new ArrayList<ProgramaEspecialFinanciamentoBean>();
    private String currentState = PESQUISAR_STATE;
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String EDITAR_STATE = "editar";
    public static final String ADICIONAR_STATE = "adicionar";
    private boolean podeEditarFinanciamentoEspecial;
    private boolean podeCriarFinanciamentoEspecial;
    private boolean podeExcluirFinanciamentoEspecial;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private String imagemExcluirRegistro = "/images/icons/del.png";
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;

    public ProgramaEspecialFinanciamentoManagedBean() {
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        podeEditarFinanciamentoEspecial = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.FINANCIAMENTO_ESPECIAL_EDITAR);
        podeCriarFinanciamentoEspecial = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.FINANCIAMENTO_ESPECIAL_CRIAR);
        podeExcluirFinanciamentoEspecial = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.FINANCIAMENTO_ESPECIAL_EXCLUIR);

        this.pesquisar();
    }

    public String getImagemCriarRegistro() {
        if (podeCriarFinanciamentoEspecial) {
            imagemCriarRegistro = "/images/icons/add1.png";
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarFinanciamentoEspecial) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        if (podeExcluirFinanciamentoEspecial) {
            imagemExcluirRegistro = "/images/icons/del.png";
        } else {
            imagemExcluirRegistro = "/images/icons/delbw.png";
        }
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public boolean isPodeCriarFinanciamentoEspecial() {
        return podeCriarFinanciamentoEspecial;
    }

    public void setPodeCriarFinanciamentoEspecial(boolean podeCriarFinanciamentoEspecial) {
        this.podeCriarFinanciamentoEspecial = podeCriarFinanciamentoEspecial;
    }

    public boolean isPodeEditarFinanciamentoEspecial() {
        return podeEditarFinanciamentoEspecial;
    }

    public void setPodeEditarFinanciamentoEspecial(boolean podeEditarFinanciamentoEspecial) {
        this.podeEditarFinanciamentoEspecial = podeEditarFinanciamentoEspecial;
    }

    public boolean isPodeExcluirFinanciamentoEspecial() {
        return podeExcluirFinanciamentoEspecial;
    }

    public void setPodeExcluirFinanciamentoEspecial(boolean podeExcluirFinanciamentoEspecial) {
        this.podeExcluirFinanciamentoEspecial = podeExcluirFinanciamentoEspecial;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public List<ProgramaEspecialFinanciamentoBean> getListaProgramasEspeciais() {
        return listaProgramasEspeciais;
    }

    public void setListaProgramasEspeciais(List<ProgramaEspecialFinanciamentoBean> listaProgramasEspeciais) {
        this.listaProgramasEspeciais = listaProgramasEspeciais;
    }

    public ProgramaEspecialFinanciamentoBean getProgramaEspecial() {
        return programaEspecial;
    }

    public void setProgramaEspecial(ProgramaEspecialFinanciamentoBean programaEspecial) {
        this.programaEspecial = programaEspecial;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        this.programaEspecial = new ProgramaEspecialFinanciamentoBean();

        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaProgramasEspeciais = Service.getInstance().listarProgramaEspecialFinanciamento();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaProgramasEspeciais = Service.getInstance().pesquisarProgramaEspecialFinanciamento(pesquisa);
        }
    }

    public void prepareEditar(ProgramaEspecialFinanciamentoBean prg) {
        this.setCurrentState(EDITAR_STATE);
        this.programaEspecial = prg;
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }
    
    public void prepareExcluir(ProgramaEspecialFinanciamentoBean prg) {
        this.programaEspecial = prg;
        RequestContext.getCurrentInstance().execute("PF('deletePopup').show();");
    }

    public void prepareAdicionar() {
        this.clear();
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public void clear() {
        this.programaEspecial = new ProgramaEspecialFinanciamentoBean();
        //this.cleanSubmittedValues(this.panelForm);
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            //this.fornecedor.setFkEndereco(fornecedor.getFkEndereco());
            Service.getInstance().cadastraProgramaEspecialFinanciamento(programaEspecial);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarProgramaEspecialFinanciamento(programaEspecial);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }

    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        try{
            Service.getInstance().excluirProgramaEspecialFinanciamento(programaEspecial);
        }catch(Exception e){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Impossível excluir registro", "O Programa Especial de Financiamento tem auxílios vinculados.");
            FacesContext.getCurrentInstance().addMessage("", message);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioProgramasEspeciais(listaProgramasEspeciais, ReportService.FORMATO_PDF);
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO XLS");
        InputStream stream = ReportService.getInstance().emiteRelatorioProgramasEspeciais(listaProgramasEspeciais, ReportService.FORMATO_XLS);
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }
}
