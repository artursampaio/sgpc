/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.ContaCorrenteBean;
import br.usp.icmc.sgpc.beans.InstituicaoBancariaBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "addEditContaCorrenteMB")
@ViewScoped
public class AddEditContaCorrenteDialog implements Serializable{
    private static final Logger logger = Logger.getLogger(AddEditContaCorrenteDialog.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    
    //private List<ContaCorrenteBean> listaContaCorrente = new ArrayList<ContaCorrenteBean>();
    private List<InstituicaoBancariaBean> listaInstituicaoBancaria = new ArrayList<InstituicaoBancariaBean>();
    private ContaCorrenteBean contaCorrenteCadastrar;
    private PessoaBean responsavelCC;
    
    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        listaInstituicaoBancaria.addAll(Service.getInstance().listarInstituicoesBancarias());
        listaInstituicaoBancaria.add(0, Service.getInstance().buscarInstituicaoBancaria(43)); // adiciona banco brasil em primeiro da lista

    }

    public ContaCorrenteBean getContaCorrenteCadastrar() {
        return contaCorrenteCadastrar;
    }

    public void setContaCorrenteCadastrar(ContaCorrenteBean contaCorrenteCadastrar) {
        this.contaCorrenteCadastrar = contaCorrenteCadastrar;
    }

//    public List<ContaCorrenteBean> getListaContaCorrente() {
//        return listaContaCorrente;
//    }
//
//    public void setListaContaCorrente(List<ContaCorrenteBean> listaContaCorrente) {
//        this.listaContaCorrente = listaContaCorrente;
//    }

    public PessoaBean getResponsavelCC() {
        return responsavelCC;
    }

    public void setResponsavelCC(PessoaBean responsavelCC) {
        logger.debug("============ setResponsavelCC");
        logger.debug("============ "+responsavelCC);
        this.responsavelCC = responsavelCC;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public List<InstituicaoBancariaBean> getListaInstituicaoBancaria() {
        return listaInstituicaoBancaria;
    }

    public void setListaInstituicaoBancaria(List<InstituicaoBancariaBean> listaInstituicaoBancaria) {
        this.listaInstituicaoBancaria = listaInstituicaoBancaria;
    }
    
    
    
    public void prepareAdicionarCC(){
        this.currentState = ADICIONAR_STATE;
        contaCorrenteCadastrar = new ContaCorrenteBean();
        contaCorrenteCadastrar.setAtiva(true);
//        if(indicarBolsista){
//            this.responsavelCC = auxilio.getFkBolsista();
//        }else{
//            this.responsavelCC = responsavel;
//        }
        RequestContext.getCurrentInstance().execute("PF('addEditPopupCorrente').show();");
    }
    
    public void prepareEditarCC(Integer idContaEditar){
        this.currentState = EDITAR_STATE;
        this.contaCorrenteCadastrar = Service.getInstance().buscarContaCorrente(idContaEditar);
        RequestContext.getCurrentInstance().execute("PF('addEditPopupCorrente').show();");
    }
    public void prepareExcluirCC(Integer idContaEditar){
        this.currentState = EDITAR_STATE;
        this.contaCorrenteCadastrar = Service.getInstance().buscarContaCorrente(idContaEditar);
        RequestContext.getCurrentInstance().execute("PF('deletePopup').show();");
    }
    
    public void gravarCC() {
        if(ADICIONAR_STATE.equalsIgnoreCase(currentState)){
            this.contaCorrenteCadastrar.setFkPessoa(responsavelCC);
            responsavelCC.getContasCorrentes().add(this.contaCorrenteCadastrar);
            responsavelCC = Service.getInstance().atualizarPessoa(responsavelCC);
        }else{
            contaCorrenteCadastrar = Service.getInstance().atualizarContaCorrente(contaCorrenteCadastrar);
            //responsavelCC = Service.getInstance().atualizarPessoa(responsavelCC);
        }
        RequestContext.getCurrentInstance().execute("PF('addEditPopupCorrente').hide();");
//        this.listaContaCorrente = Service.getInstance().pesquisarContasCorrentes(responsavelCC);
    }
}
