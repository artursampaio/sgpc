/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AlineaBean;
import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.HistoricoAuxilioBean;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "liberarVerbaMB")
@ViewScoped
public class LiberarVerbaManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(LiberarVerbaManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private HistoricoAuxilioBean historicoAuxilioBean;
    private AlineaBean alineaBean;
    private DespesaBean despesa;
    
    @PostConstruct
    public void postConstruct() {
        historicoAuxilioBean = new HistoricoAuxilioBean();
    }
    
    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public void prepareAdicionar() {
        this.setCurrentState(ADICIONAR_STATE);
        historicoAuxilioBean = new HistoricoAuxilioBean();
    }

    public void prepareEditar() {
        this.setCurrentState(EDITAR_STATE);
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public HistoricoAuxilioBean getHistoricoAuxilioBean() {
        return historicoAuxilioBean;
    }

    public void setHistoricoAuxilioBean(HistoricoAuxilioBean historicoAuxilioBean) {
        this.historicoAuxilioBean = historicoAuxilioBean;
    }

    public AlineaBean getAlineaBean() {
        return alineaBean;
    }

    public void setAlineaBean(AlineaBean alineaBean) {
        this.alineaBean = alineaBean;
    }
    
    public void liberarVerba() {
        // registrar historico
        Service.getInstance().registrarHistoricoAuxilio(historicoAuxilioBean);
        this.alineaBean = new AlineaBean();
        //pesquisarHistorico();
        if(despesa != null){
            despesa.setVerbaLiberada(true);
            Service.getInstance().atualizarDespesa(despesa);
        }
        RequestContext.getCurrentInstance().execute("PF('liberarVerbaPanel').hide();");
    }
    
    public void prepareLiberarVerba(AlineaBean alinea) {
        this.alineaBean = alinea;
        this.despesa = null;
        historicoAuxilioBean = new HistoricoAuxilioBean();
        historicoAuxilioBean.setFkAuxilio(userSessionMB.getAuxilio());
        historicoAuxilioBean.setAcao("Liberação de verba.");
        historicoAuxilioBean.setDescricao("Liberação de verba.");
        historicoAuxilioBean.setDataAlteracao(new Date());
        historicoAuxilioBean.setFkResponsavel(userSessionMB.getLoggedUser());
        if(alineaBean !=null && alineaBean.getFkCategoriaAlinea()!=null){
            historicoAuxilioBean.setNomeAlinea(alineaBean.getFkCategoriaAlinea().getNome());
        } else {
            historicoAuxilioBean.setNomeAlinea("N/A");
        }
        RequestContext.getCurrentInstance().execute("PF('liberarVerbaPanel').show();");
    }
    
    public void prepareLiberarVerba(DespesaBean despesa) {
        prepareLiberarVerba(despesa.getFkAlinea());
        historicoAuxilioBean.setValor(despesa.getValor());
        this.despesa = despesa;
        RequestContext.getCurrentInstance().execute("PF('liberarVerbaPanel').show();");
    }
    
}
