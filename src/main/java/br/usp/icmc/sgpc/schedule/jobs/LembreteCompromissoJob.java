/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.schedule.jobs;

import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.ConfiguracaoBean;
import br.usp.icmc.sgpc.beans.ItemBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PrestacaoContasBean;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.beans.RelatorioCientificoBean;
import br.usp.icmc.sgpc.beans.SolicitacaoCotacaoBean;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import br.usp.icmc.sgpc.service.AuditoriaService;
import br.usp.icmc.sgpc.service.ConfiguracaoService;
import br.usp.icmc.sgpc.service.MailService;
import br.usp.icmc.sgpc.service.Service;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author Artur
 */
public class LembreteCompromissoJob extends QuartzJobBean {

    private static Logger logger = Logger.getLogger(LembreteCompromissoJob.class);
    private int timeout;
    private String message;

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public void setMessage(String value) {
        message = value;
    }

    public void enviarPrestacaoContasERelatorioCientifico() {
        //Envio de prestações de contas e relatórios científicos para os respectivos responsáveis (sem ser o financeiro)
        logger.debug("Iniciando tarefa agendada");
        List<PrestacaoContasBean> listaPrestacoes = null;
        List<RelatorioCientificoBean> listaRelatorios = null;
        DateFormat formatadorData = new SimpleDateFormat("dd/MM/yyyy");

        String subject = "Lembrete de compromissos SGPC";
        //StringBuilder recipient = new StringBuilder();
        String recipient = "artur@icmc.usp.br";
        StringBuilder message = null;
        //boolean temPendencias = false;

        Calendar dtInicioPeriodo = GregorianCalendar.getInstance();
        dtInicioPeriodo.add(Calendar.DAY_OF_MONTH, 15);
        //dtInicioPeriodo.add(Calendar.MONTH, 1); -> Outra forma de ir para o próximo mês
        dtInicioPeriodo.set(Calendar.DAY_OF_MONTH, 1);

        Calendar dtFimPeriodo = GregorianCalendar.getInstance();
        dtFimPeriodo.add(Calendar.DAY_OF_MONTH, 15);
        dtFimPeriodo.set(Calendar.DAY_OF_MONTH, dtFimPeriodo.getMaximum(Calendar.DAY_OF_MONTH));

        //Calendar c = new GregorianCalendar();

        List<PessoaBean> listaPessoas = Service.getInstance().listarPessoas();
        logger.debug("  --> Pessoas:" + listaPessoas.size());
        for (PessoaBean pessoa : listaPessoas) {
            message = new StringBuilder();

            recipient = "artur@icmc.usp.br";

            //pessoa.getEmail();

            listaPrestacoes = Service.getInstance().getPrestacoesContas(pessoa, dtInicioPeriodo.getTime(), dtFimPeriodo.getTime());
            listaRelatorios = Service.getInstance().getRelatoriosCientificos(pessoa, dtInicioPeriodo.getTime(), dtFimPeriodo.getTime());

            if (listaRelatorios.size() > 0 || listaPrestacoes.size() > 0) {

                message.append("Caro ");
                message.append(pessoa.getNome());
                message.append(", a seguir são listadas as prestações de contas a serem realizadas entre ");

                message.append(formatadorData.format(dtInicioPeriodo.getTime()));
                message.append(" e ");
                message.append(formatadorData.format(dtFimPeriodo.getTime()));

                message.append("\n");
                message.append("\n");
                message.append("\n");
                message.append("----------------------------------------------------------------------\n");
                message.append("Prestações de contas\n");
                for (PrestacaoContasBean prestacao : listaPrestacoes) {
                    message.append("----------------------------------------------------------------------\n");
                    message.append("Data: ");
                    message.append(formatadorData.format(prestacao.getDataLimite()));
                    message.append("\n");
                    message.append("Financiador: ");
                    message.append(prestacao.getFkAuxilio().getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getNome());
                    message.append("\t");
                    message.append("Processo: ");
                    message.append(prestacao.getFkAuxilio().getProtocoloFinanciador());
                    message.append("\n");
                    message.append("Identificação: ");
                    message.append(prestacao.getFkAuxilio().getNomeIdentificacao());
                    message.append("\n");
                    message.append("Descrição: ");
                    message.append(prestacao.getDescricao());
                    message.append("\n");
                }

                message.append("\n");
                message.append("----------------------------------------------------------------------\n");
                message.append("Relatórios científicos\n");
                for (RelatorioCientificoBean relatorio : listaRelatorios) {
                    message.append("----------------------------------------------------------------------\n");
                    message.append("Data: ");
                    message.append(formatadorData.format(relatorio.getDataLimite()));
                    message.append("\n");
                    message.append("Financiador: ");
                    message.append(relatorio.getFkAuxilio().getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getNome());
                    message.append("\t");
                    message.append("Processo: ");
                    message.append(relatorio.getFkAuxilio().getProtocoloFinanciador());
                    message.append("\n");
                    message.append("Identificação: ");
                    message.append(relatorio.getFkAuxilio().getNomeIdentificacao());
                    message.append("\n");
                    message.append("Descrição: ");
                    message.append(relatorio.getDescricao());
                    message.append("\n");
                }
                message.append("\n\n\n");
                logger.debug("Mensagem enviada \n \n \n " + message);

                MailService.getInstance().sendMail(subject, recipient, message.toString());
                AuditoriaService.getInstance().gravarAcaoUsuario(pessoa, "Envio de Email", "Compromisso Job - Prestacao de Contas e Relatórios Científicos", "Email enviado para: " + pessoa.getEmail() + ". " + (listaRelatorios.size() + listaPrestacoes.size()) + " compromissos (prestações de contas e relatórios científicos) a vencer entre " + dtInicioPeriodo.getTime() + " e " + dtFimPeriodo.getTime() + ".", ConfigConstantes.CONFIG_AUDITORIA_EMAIL_CONTAS_VENCER);
            }
        }
    }

    public static void enviarPrestacaoContas() {
        logger.debug("Iniciando o método prestacao de contas");
        List<PrestacaoContasBean> listaPrestacoes = null;
        DateFormat formatadorData = new SimpleDateFormat("dd/MM/yyyy");

        Calendar dtInicioPeriodo = GregorianCalendar.getInstance();
        dtInicioPeriodo.add(Calendar.DAY_OF_MONTH, 15);
        //dtInicioPeriodo.add(Calendar.MONTH, 1); -> Outra forma de ir para o próximo mês
        dtInicioPeriodo.set(Calendar.DAY_OF_MONTH, 1);

        Calendar dtFimPeriodo = GregorianCalendar.getInstance();
        dtFimPeriodo.add(Calendar.DAY_OF_MONTH, 15);
        dtFimPeriodo.set(Calendar.DAY_OF_MONTH, dtFimPeriodo.getMaximum(Calendar.DAY_OF_MONTH));

        ConfiguracaoBean notificacaoPR = ConfiguracaoService.getInstance().getConfiguracao(ConfigConstantes.CONFIG_NOTIFICACAO_PRESTACAO_RELATORIO);
        //Cria uma lista de prestações de contas que vençam em determinado período.
        listaPrestacoes = Service.getInstance().getPrestacoesContas(dtInicioPeriodo.getTime(), dtFimPeriodo.getTime());

        logger.debug("Quantidade de prestações de contas a realizar:" + listaPrestacoes.size());

        if (listaPrestacoes.size() > 0) {

            String subject = "Lembrete de compromissos SGPC";
            StringBuilder recipient = new StringBuilder();
            StringBuilder message = null;

            List<PessoaBean> listaPessoasFinanceiro = Service.getInstance().pesquisarPessoasFinanceiro();
            logger.debug("Quantidade de pessoas na lista do financeiro:" + listaPessoasFinanceiro.size());

            StringTokenizer str = new StringTokenizer(notificacaoPR.getValor());
            while (str.hasMoreElements()) {
                recipient.append(",");
                recipient.append(str.nextToken());
            }
            recipient.append(",artur@icmc.usp.br");
            /* for (PessoaBean pessoa : listaPessoasFinanceiro) {
            recipient.append(",");
            recipient.append(pessoa.getEmail());
            }*/

            message = new StringBuilder();

            message.append("A seguir são listadas as prestações de contas a serem realizadas entre ");
            message.append(formatadorData.format(dtInicioPeriodo.getTime()));
            message.append(" e ");
            message.append(formatadorData.format(dtFimPeriodo.getTime()));

            message.append("\n");
            message.append("\n");
            message.append("----------------------------------------------------------------------\n");
            message.append("Prestações de contas\n");
            for (PrestacaoContasBean prestacao : listaPrestacoes) {
                message.append("----------------------------------------------------------------------\n");
                message.append("Data: ");
                message.append(formatadorData.format(prestacao.getDataLimite()));
                message.append("\n");
                message.append("Outorgado: ");
                message.append(prestacao.getFkAuxilio().getFkProjeto().getFkResponsavel().getNome());
                message.append("\n");
                message.append("Financiador: ");
                message.append(prestacao.getFkAuxilio().getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getNome());
                message.append("\t");
                message.append("Processo: ");
                message.append(prestacao.getFkAuxilio().getProtocoloFinanciador());
                message.append("\n");
                message.append("Identificação: ");
                message.append(prestacao.getFkAuxilio().getNomeIdentificacao());
                message.append("\n");
                message.append("Descrição: ");
                message.append(prestacao.getDescricao());
                message.append("\n");
                message.append("Responsável: ");
                if (prestacao.getFkResponsavel() != null) {
                    message.append(prestacao.getFkResponsavel().getNome());
                } else {
                    message.append("N/A");
                }
                message.append("\n");
                message.append("\n");
            }

            MailService.getInstance().sendMail(subject, recipient.toString(), message.toString());
            //AuditoriaService.getInstance().gravarAcaoPessoa(pessoa, "Email enviado para ATAf. " + listaPrestacoes.size() + " prestações de contas a vencer entre "+dtInicioPeriodo.getTime()+" e "+dtFimPeriodo.getTime()+".");
        }
    }

    public LembreteCompromissoJob() {
    }

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void verificaSolicitacaoCotacao() {
        List<SolicitacaoCotacaoBean> listaSolicitacao = Service.getInstance().listarSolicitacaoCotacao();

        List<SolicitacaoCotacaoBean> listaEfetuadas = new ArrayList<SolicitacaoCotacaoBean>();
        List<SolicitacaoCotacaoBean> listaInvalidas = new ArrayList<SolicitacaoCotacaoBean>();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date hoje = new Date();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(hoje);
        gc.add(GregorianCalendar.DATE, -1);
        hoje = gc.getTime();

         ConfiguracaoBean notificacaoCotacao = ConfiguracaoService.getInstance().getConfiguracao(ConfigConstantes.CONFIG_NOTIFICACAO_COTACAO);

        for (int i = 0; i < listaSolicitacao.size(); i++) {
            SolicitacaoCotacaoBean solicitacao = listaSolicitacao.get(i);
            if (!solicitacao.getFkItem().getStatus().equals("Anulado") || !solicitacao.getFkItem().getStatus().equals("Cotado")) {
                if (sdf.format(solicitacao.getDataLimiteResposta()).equals(sdf.format(hoje))) {
                    if (solicitacao.getOrcamentos().size() >= 3) {
                        ItemBean item = solicitacao.getFkItem();
                        item.setStatus("Cotado");
                        Service.getInstance().atualizarItem(item);
                        listaEfetuadas.add(solicitacao);
                    } else {
                        ItemBean item = solicitacao.getFkItem();
                        item.setStatus("Anulado");
                        Service.getInstance().atualizarItem(item);
                        listaInvalidas.add(solicitacao);
                    }
                }
            }
        }

        if (listaEfetuadas.size() > 0 || listaInvalidas.size() > 0) {

            String subject = "Solicitação de Cotação - SGPC";
            StringBuilder recipient = new StringBuilder();
            StringBuilder message = null;

            List<PessoaBean> listaPessoasFinanceiro = Service.getInstance().pesquisarPessoasFinanceiro();
            logger.debug("Quantidade de pessoas na lista do financeiro:" + listaPessoasFinanceiro.size());

            recipient.append("herick.marques@gmail.com");
            /* for (PessoaBean pessoa : listaPessoasFinanceiro) {
            recipient.append(",");
            recipient.append(pessoa.getEmail());
            }*/
            StringTokenizer str = new StringTokenizer(notificacaoCotacao.getValor());
            while (str.hasMoreElements()) {
                recipient.append(",");
                recipient.append(str.nextToken());
            }

            message = new StringBuilder();

            if (listaEfetuadas.size() > 0) {
                message.append("As seguintes cotações foram finalizadas:\n\n");

                for (int i = 0; i < listaEfetuadas.size(); i++) {
                    SolicitacaoCotacaoBean solicitacao = listaEfetuadas.get(i);
                    message.append("----------------------------------------------------------------------\n");
                    message.append("Item: ").append(solicitacao.getFkItem().getFkProduto().getDescricao());
                    message.append("\n");
                    message.append("Referente ao projeto: ").append(solicitacao.getFkItem().getFkCotacao().getFkProjeto().getTitulo());
                    message.append("\n\n");
                }
                message.append("\n\n");
            }

            if (listaInvalidas.size() > 0) {
                message.append("As seguintes cotações foram Anuladas por não possuírem o limite mínimo de 3 orçamentos:\n\n");

                for (int i = 0; i < listaInvalidas.size(); i++) {
                    SolicitacaoCotacaoBean solicitacao = listaInvalidas.get(i);
                    message.append("----------------------------------------------------------------------\n");
                    message.append("Item: ").append(solicitacao.getFkItem().getFkProduto().getDescricao());
                    message.append("\n");
                    message.append("Referente ao projeto: ").append(solicitacao.getFkItem().getFkCotacao().getFkProjeto().getTitulo());
                    message.append("\n\n");
                }
                message.append("\n\n");
            }
            MailService.getInstance().sendMail(subject, recipient.toString(), message.toString());
        }
    }

    public void atualizarStatus() {

        logger.debug("Iniciando tarefa agendada -- Atualização de Status");

        List<PrestacaoContasBean> listaPrestacoes = null;
        List<RelatorioCientificoBean> listaRelatorios = null;
        List<ProjetoBean> listaProjetos = null;
        List<AuxilioBean> listaAuxilios = null;

        listaPrestacoes = Service.getInstance().listarPrestacoesContas();
        listaRelatorios = Service.getInstance().listarRelatoriosCientificos();
        listaProjetos = Service.getInstance().listarProjetos();
        listaAuxilios = Service.getInstance().listarAuxilios();

        Date hoje = new Date();

        for (int i = 0; i < listaPrestacoes.size(); i++) {
            PrestacaoContasBean prestacao = listaPrestacoes.get(i);
            Service.getInstance().atualizarStatusPrestacaoContas(prestacao);
            Service.getInstance().atualizarPrestacaoContas(prestacao);
        }

        for (int i = 0; i < listaRelatorios.size(); i++) {
            RelatorioCientificoBean relatorio = listaRelatorios.get(i);
            Service.getInstance().atualizarStatusRelatorioCientifico(relatorio);
            Service.getInstance().atualizarRelatorioCientifico(relatorio);
        }

        for (int i = 0; i < listaProjetos.size(); i++) {
            ProjetoBean projeto = listaProjetos.get(i);
            Service.getInstance().atualizarStatusProjeto(projeto);
            Service.getInstance().atualizarProjeto(projeto);
        }

        for (int i = 0; i < listaAuxilios.size(); i++) {
            AuxilioBean auxilio = listaAuxilios.get(i);
            Service.getInstance().atualizarStatusAuxilio(auxilio);
            Service.getInstance().atualizarAuxilio(auxilio);
        }
    }
    /*
     * Verifica se a partir da data corrente contados 7 dias existe projeto ou auxílio com DataFinal igual.
     *
     */

    public void verificarFimProjetoAuxilio() {

        logger.debug("Iniciando tarefa agendada -- Verificacao de Projetos e Auxilios a finalizar");

        List<ProjetoBean> listaProjetos = null;
        List<AuxilioBean> listaAuxilios = null;

        listaProjetos = Service.getInstance().listarProjetos();
        listaAuxilios = Service.getInstance().listarAuxilios();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        Date hoje = new Date();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(hoje);
        gc.add(GregorianCalendar.DATE, 7);
        hoje = gc.getTime();

        for (int i = 0; i < listaProjetos.size(); i++) {
            ProjetoBean projeto = listaProjetos.get(i);
            if (projeto.getStatus().equals("Em Planejamento") || projeto.getStatus().equals("Em Andamento")) {
                if (sdf.format(hoje).equals(sdf.format(projeto.getDataFinal()))) {

                    String subject = "Aviso sobre Projeto - SGPC";
                    StringBuilder recipient = new StringBuilder();
                    StringBuilder message = null;

                    recipient.append("herick.marques@gmail.com");
                    //recipient.append(projeto.getFkResponsavel().getEmail());
                    message = new StringBuilder();

                    message.append("O seguinte projeto terá seu status alterado para atrasado em 7 dias:\n\n");
                    message.append("----------------------------------------------------------------------\n");
                    message.append("Projeto: ").append(projeto.getTitulo());
                    message.append("\n");
                    message.append("Data Inicial: ").append(sdf.format(projeto.getDataInicial()));
                    message.append("\n");
                    message.append("Data Final: ").append(sdf.format(projeto.getDataFinal()));
                    message.append("\n\n");

                    MailService.getInstance().sendMail(subject, recipient.toString(), message.toString());
                }
            }
        }

        for (int i = 0; i < listaAuxilios.size(); i++) {
            AuxilioBean auxilio = listaAuxilios.get(i);
            if (auxilio.getStatus().equals("Em Elaboração") || auxilio.getStatus().equals("Em Análise") || auxilio.getStatus().equals("Aprovado") || auxilio.getStatus().equals("Em Andamento")) {
                if (sdf.format(hoje).equals(sdf.format(auxilio.getDataFinal()))) {

                    String subject = "Aviso sobre Auxílio - SGPC";
                    StringBuilder recipient = new StringBuilder();
                    StringBuilder message = null;

                    recipient.append("herick.marques@gmail.com");
                    //recipient.append(auxilio.getFkProjeto().getFkResponsavel().getEmail());
                    message = new StringBuilder();

                    message.append("O seguinte auxílio terá seu status alterado para atrasado em 7 dias:\n\n");
                    message.append("----------------------------------------------------------------------\n");
                    message.append("Auxílio: ").append(auxilio.getNomeIdentificacao());
                    message.append("\n");
                    message.append("Referente ao projeto:").append(auxilio.getFkProjeto().getTitulo());
                    message.append("\n");
                    message.append("Data Inicial: ").append(sdf.format(auxilio.getDataInicial()));
                    message.append("\n");
                    message.append("Data Final: ").append(sdf.format(auxilio.getDataFinal()));
                    message.append("\n\n");

                    MailService.getInstance().sendMail(subject, recipient.toString(), message.toString());
                }
            }
        }
    }
}
